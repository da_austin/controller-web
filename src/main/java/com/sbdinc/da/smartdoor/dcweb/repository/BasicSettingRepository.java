package com.sbdinc.da.smartdoor.dcweb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sbdinc.da.smartdoor.dcweb.model.BasicSetting;

/**
 *
 *
 * @author Dian Jiao
 *
 */
public interface BasicSettingRepository extends JpaRepository<BasicSetting, Integer> {

}
