package com.sbdinc.da.smartdoor.dcweb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sbdinc.da.smartdoor.dcweb.model.TechVisit;

@Repository
public interface TechVisitRepository extends JpaRepository<TechVisit, Integer> {

}
