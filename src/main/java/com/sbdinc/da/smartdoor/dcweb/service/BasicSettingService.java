package com.sbdinc.da.smartdoor.dcweb.service;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/**
 *
 *
 * @author Dian Jiao
 *
 */
public interface BasicSettingService {
	int saveBasicSetting(String xml, long msgId, long visitId) throws ParserConfigurationException, SAXException, IOException;
}
