package com.sbdinc.da.smartdoor.dcweb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sbdinc.da.smartdoor.dcweb.model.CustDim;

/**
 *
 *
 * @author Dian Jiao
 *
 */
public interface CustDimRepository extends JpaRepository<CustDim, Integer> {

}
