package com.sbdinc.da.smartdoor.dcweb.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 *
 * @author Dian Jiao
 *
 */

@Entity
@Table(name = "ctrlr_basic_setng")
public class BasicSetting implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ctrlr_basic_setng_id")
    private int id;
	
	@Column(name = "msg_id")
	private long messageId;
	
	@Column(name = "visit_id")
	private long visitId;
	
	@Column(name = "ctrlr_serl_nbr")
	private String controllerSerialNumber;
	
	@Column(name = "door_id")
	private String doorId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "read_dtm")
	private Date readDt;
	
	@Column(name = "op_code")
	private int opCode;
	
	@Column(name = "handing_1")
	private int handing1;
	
	@Column(name = "handing_2")
	private int handing2;
	
	@Column(name = "safety_logic")
	private int safetyLogic;
	
	@Column(name = "hold_beam_type")
	private int holdBeamType;
	
	@Column(name = "function_switch_type")
	private int functionSwitchType;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "insert_ts", insertable=true, updatable=false)
	private Date insertTs;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_ts", insertable=false, updatable=true)
	private Date updateTs;
	
	public int getId() {
		return id;
	}

	public long getMessageId() {
		return messageId;
	}

	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}

	public long getVisitId() {
		return visitId;
	}

	public void setVisitId(long visitId) {
		this.visitId = visitId;
	}

	public String getControllerSerialNumber() {
		return controllerSerialNumber;
	}

	public void setControllerSerialNumber(String controllerSerialNumber) {
		this.controllerSerialNumber = controllerSerialNumber;
	}

	public String getDoorId() {
		return doorId;
	}

	public void setDoorId(String doorId) {
		this.doorId = doorId;
	}

	public Date getReadDt() {
		return readDt;
	}

	public void setReadDt(Date readDt) {
		this.readDt = readDt;
	}

	public int getOpCode() {
		return opCode;
	}

	public void setOpCode(int opCode) {
		this.opCode = opCode;
	}

	public int getHanding1() {
		return handing1;
	}

	public void setHanding1(int handing1) {
		this.handing1 = handing1;
	}

	public int getHanding2() {
		return handing2;
	}

	public void setHanding2(int handing2) {
		this.handing2 = handing2;
	}

	public int getSafetyLogic() {
		return safetyLogic;
	}

	public void setSafetyLogic(int safetyLogic) {
		this.safetyLogic = safetyLogic;
	}

	public int getHoldBeamType() {
		return holdBeamType;
	}

	public void setHoldBeamType(int holdBeamType) {
		this.holdBeamType = holdBeamType;
	}

	public int getFunctionSwitchType() {
		return functionSwitchType;
	}

	public void setFunctionSwitchType(int functionSwitchType) {
		this.functionSwitchType = functionSwitchType;
	}

	public Date getInsertTs() {
		return insertTs;
	}

	public void setInsertTs(Date insertTs) {
		this.insertTs = insertTs;
	}

	public Date getUpdateTs() {
		return updateTs;
	}

	public void setUpdateTs(Date updateTs) {
		this.updateTs = updateTs;
	}
	
}
