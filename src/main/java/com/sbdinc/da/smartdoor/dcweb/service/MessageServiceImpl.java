package com.sbdinc.da.smartdoor.dcweb.service;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbdinc.da.smartdoor.dcweb.model.Message;
import com.sbdinc.da.smartdoor.dcweb.repository.MessageRepository;

/**
 * Implementation class for Message Service
 * 
 * @author Dian Jiao
 *
 */

@Service
public class MessageServiceImpl implements MessageService {
	private static final Logger logger = LoggerFactory.getLogger(MessageService.class);
	@Autowired
	MessageRepository messageRepository;
	
	/**
	 * Persist Message entity
	 * 
	 * @param source  Message source, e.g. "mobile"
	 * @param type  Message type, e.g. "cycle count"
	 * @param format Message type, e.g. "json"
	 * @param msgString Message body
	 * @return  Row ID of message record
	 */
	@Override
	public long saveMessage(String source, String type, String format, String trigger, String msgBody) {
		Message message = new Message();
		Date dt = new Date();
		message.setSourceCode(source);
		message.setType(type);
		message.setFormat(format);
		message.setMsgRecvDt(dt);
		message.setTrigger(trigger);
		message.setMsgBody(msgBody);
		messageRepository.saveAndFlush(message);
		logger.info(message.toString());
		return message.getId();
	}

	@Override
	public void updateMessage(long msgId, char code, String note) {
		messageRepository.updateLoadStatus(msgId, code, note);
		
	}

	
}
