package com.sbdinc.da.smartdoor.dcweb.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entity class for ErrorHistory
 * 
 * @author Dian Jiao
 *
 */
@Entity
@Table(name = "CTRLR_ERR_HIST")
public class ErrorHistory implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ERR_ID")
    private long id;
	
	@Column(name = "MSG_ID")
	private long messageId;
	
	@Column(name = "VISIT_ID")
	private long visitId;
	
	@Column(name = "DOOR_ID")
	private String doorId;
	
	@Column(name = "CTRLR_SERL_NBR")
	private String controllerSerialNumber;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "HIST_RESET_DTM")
	private Date resetDt;
	
	@Column(name = "CTRLR_CYCLE_CNT")
	private int controllerCycleCount;

	@Column(name = "POWERED_UP_DYS")
	private int days;
	
	@Column(name = "ERR_DTM")
	private Date errDt;
	
	@Column(name = "ERR_TIMEZONE")
	private int errTz;
	
	@Column(name = "ERR_CD")
	private String errCode;
	
	@Column(name = "ERR_NAME")
	private String errName;
	
	@Column(name = "ERR_DESC")
	private String description;
	
	@Column(name = "M1_ERR_CNT")
	private int m1Count;
	
	@Column(name = "M1_ERR_CYCLE_CNT")
	private int m1CycleCount;
	
	@Column(name = "M2_ERR_CNT")
	private int m2Count;
	
	@Column(name = "M2_ERR_CYCLE_CNT")
	private int m2CycleCount;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ETL_INSERT_TS", insertable=true, updatable=false)
	private Date insertTS;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ETL_UPDATE_TS", insertable=false, updatable=true)
	private Date updateTS;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getMessageId() {
		return messageId;
	}

	public void setMessageID(long messageId) {
		this.messageId = messageId;
	}

	public long getVisitID() {
		return visitId;
	}

	public void setVisitID(long visitId) {
		this.visitId = visitId;
	}

	public String getControllerSerialNumber() {
		return controllerSerialNumber;
	}

	public void setControllerSerialNumber(String controllerSerialNumber) {
		this.controllerSerialNumber = controllerSerialNumber;
	}

	public String getDoorId() {
		return doorId;
	}

	public void setDoorId(String doorId) {
		this.doorId = doorId;
	}

	public Date getResetDt() {
		return resetDt;
	}

	public void setResetDt(Date resetDt) {
		this.resetDt = resetDt;
	}

	public int getControllerCycleCount() {
		return controllerCycleCount;
	}

	public void setControllerCycleCount(int controllerCycleCount) {
		this.controllerCycleCount = controllerCycleCount;
	}

	public int getDays() {
		return days;
	}

	public void setDays(int days) {
		this.days = days;
	}

	public Date getErrDt() {
		return errDt;
	}

	public void setErrDt(Date errDt) {
		this.errDt = errDt;
	}

	public int getErrTz() {
		return errTz;
	}

	public void setErrTz(int errTz) {
		this.errTz = errTz;
	}

	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	public String getErrName() {
		return errName;
	}

	public void setErrName(String errName) {
		this.errName = errName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getM1Count() {
		return m1Count;
	}

	public void setM1Count(int m1Count) {
		this.m1Count = m1Count;
	}

	public int getM1CycleCount() {
		return m1CycleCount;
	}

	public void setM1CycleCount(int m1CycleCount) {
		this.m1CycleCount = m1CycleCount;
	}

	public int getM2Count() {
		return m2Count;
	}

	public void setM2Count(int m2Count) {
		this.m2Count = m2Count;
	}

	public int getM2CycleCount() {
		return m2CycleCount;
	}

	public void setM2CycleCount(int m2CycleCount) {
		this.m2CycleCount = m2CycleCount;
	}

	public Date getInsertTS() {
		return insertTS;
	}

	public void setInsertTS(Date insertTS) {
		this.insertTS = insertTS;
	}

	public Date getUpdateTS() {
		return updateTS;
	}

	public void setUpdateTS(Date updateTS) {
		this.updateTS = updateTS;
	}
	
	public String toString() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		StringBuilder sb = new StringBuilder();
		sb.append("doorID: ").append(doorId);
		sb.append("\ndays: ").append(days);
		sb.append("\ndescription: ").append(description);
		sb.append("\nerrCode: ").append(errCode);
		sb.append("\nerrDt: ").append(df.format(errDt));
		sb.append("\nerrName: ").append(errName);
		sb.append("\nm1Count: ").append(m1Count);
		sb.append("\nm1CycleCount: ").append(m1CycleCount);
		sb.append("\nm2Count: ").append(m2Count);
		sb.append("\nm2CycleCount: ").append(m2CycleCount);
		return sb.toString();
	}
}
