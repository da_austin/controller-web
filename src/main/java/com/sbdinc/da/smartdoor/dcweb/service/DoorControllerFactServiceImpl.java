package com.sbdinc.da.smartdoor.dcweb.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbdinc.da.smartdoor.dcweb.model.DoorControllerFact;
import com.sbdinc.da.smartdoor.dcweb.repository.DoorControllerFactRepository;

/**
 * @author Dian Jiao
 *
 */
@Service
public class DoorControllerFactServiceImpl implements DoorControllerFactService {
	
	@Autowired
	private DoorControllerFactRepository doorControllerFactRepository;
	
	@Override
	public long saveControllerDetail(DoorControllerFact doorControllerFact) {
		Date dt = new Date();
		doorControllerFact.setInsertTS(dt);
		doorControllerFactRepository.save(doorControllerFact);
		return doorControllerFact.getId();
	}
	
	@Override
	public long findIDbyDoorId(String doorId) {
		DoorControllerFact controller = doorControllerFactRepository.findFirstByDoorIdOrderByInsertTsDesc(doorId);
		if (controller != null) 
			return controller.getId();
		return 0;
	}

}
