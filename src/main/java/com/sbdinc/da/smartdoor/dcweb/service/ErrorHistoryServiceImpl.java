package com.sbdinc.da.smartdoor.dcweb.service;

import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sbdinc.da.smartdoor.dcweb.model.ErrorHistory;
import com.sbdinc.da.smartdoor.dcweb.repository.ErrorHistoryRepository;
import com.sbdinc.da.smartdoor.dcweb.util.DateTimeConverter;
import com.sbdinc.da.smartdoor.dcweb.util.XMLParser;

/**
 * 
 * Service Implementation class for ErrorHistory
 * 
 * @author Dian Jiao
 *
 */
@Service
public class ErrorHistoryServiceImpl implements ErrorHistoryService {
	private static final Logger logger = LoggerFactory.getLogger(ErrorHistoryServiceImpl.class);
	
	final private SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	
	@Autowired
	DoorControllerFactService doorControllerFactService;
	
	@Autowired
	ErrorHistoryRepository errorHistoryRepository;
	
	@Override
	public int saveErrorHistory(String xml, long msgId, long visitId) throws ParserConfigurationException, SAXException, IOException {
		return parseErrorHistoryXML(xml, msgId, visitId);
	}

	/**
	 * Parse error history XML
	 * @return  
	 * @throws ParseException 
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
	 */
	private int parseErrorHistoryXML(String xml, long msgId, long visitId) throws ParserConfigurationException, SAXException, IOException {
		int errorCount = 0;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    InputSource is = new InputSource(new StringReader(xml));
	    Document doc = builder.parse(is);
	    doc.getDocumentElement().normalize();
	    Element root = doc.getDocumentElement();
	    NodeList children = root.getChildNodes();
	    
	    // parse header
	    String doorId = XMLParser.getNodeValue("activeDoorID", children);
	    long controllerId = doorControllerFactService.findIDbyDoorId(doorId);
	    
	    // Check whether the matching controller exists
	    if (controllerId == 0) {
    		logger.error("No matching controller exists in ControllerDoor table with doorId (" + doorId + ")");
	    	return 0;
	    		  	
	    }
	    String serialNumber = XMLParser.getNodeValue("controllerSerialNumber", children);
	    String date = XMLParser.getNodeValue("datetime", children);
	    int timezone = Integer.parseInt(XMLParser.getNodeValue("timezone", children));
	    int controllerCount = Integer.parseInt(XMLParser.getNodeValue("cycles", children));
	    int period = Integer.parseInt(XMLParser.getNodeValue("period", children));
	    String resetDate = XMLParser.getNodeValue("errhistoryreset", children);
	    
	    // parse elements
	    Node errorRoot = XMLParser.getNode("errors", root.getChildNodes());
	    List<Node> errors = XMLParser.getNodes("error", errorRoot.getChildNodes());
	    Date insertTs = new Date();
	    
	    for (Node error : errors) {
	    	ErrorHistory errorHist = new ErrorHistory();
	    	errorHist.setDoorId(doorId);
	    	errorHist.setControllerSerialNumber(serialNumber);
	    	errorHist.setMessageID(msgId);
	    	errorHist.setVisitID(visitId);
	    	if (!StringUtils.isEmpty(date))
	    		try {
	    			errorHist.setErrDt(FORMATTER.parse(date));
	    		} catch (ParseException e) {
	    			logger.warn("ParseException Errdt");
	    		}
	    	LocalDateTime ldt = DateTimeConverter.stringToDateTime(resetDate);
			if (ldt != null) {
				Instant instant = ldt.toInstant(ZoneOffset.UTC);
			    Date dt = Date.from(instant);
			    errorHist.setResetDt(dt);
			}
	    	errorHist.setErrTz(timezone);
	    	errorHist.setControllerCycleCount(controllerCount);
	    	errorHist.setDays(period);
	    	errorHist.setErrCode(XMLParser.getNodeValue("errorCode", error.getChildNodes()));
	    	errorHist.setErrName(XMLParser.getNodeValue("name", error.getChildNodes()));
	    	errorHist.setDescription(XMLParser.getNodeValue("description", error.getChildNodes()));
	    	Node doorRoot = XMLParser.getNode("doors", error.getChildNodes());
	    	List<Node> doors = XMLParser.getNodes("door", doorRoot.getChildNodes());
	    	if (doors.size() > 0) {
	    		Node door1 = doors.get(0);
    			errorHist.setM1Count(Integer.parseInt(XMLParser.getNodeValue("count", door1.getChildNodes())));
    			errorHist.setM1CycleCount(Integer.parseInt(XMLParser.getNodeValue("lastCycleOccurence", door1.getChildNodes())));
	    		if (doors.size() > 1) {
	    			Node door2 = doors.get(1);
	    			errorHist.setM2Count(Integer.parseInt(XMLParser.getNodeValue("count", door2.getChildNodes())));
	    			errorHist.setM2CycleCount(Integer.parseInt(XMLParser.getNodeValue("lastCycleOccurence", door2.getChildNodes())));
	    		}
	    	}
	    	errorHist.setInsertTS(insertTs);
	    	errorHistoryRepository.save(errorHist);
	    	
	    	// Increment count if persistence is successful
	    	if (errorHist.getId() > 0) {
	    		errorCount++;
	    		logger.info("Persistence of ErrorHistory record is successful with ID " + Long.toString(errorHist.getId()));
	    	}  	
	    }		    
		    
		return errorCount;
	}
	
	public List<ErrorHistory> findAllErrors() {
		return errorHistoryRepository.findTop100ByOrderByInsertTSDesc();
	}
	
	public List<ErrorHistory> findErrorsByDoorId(String doorId) {
		return errorHistoryRepository.findByDoorId(doorId);
	}
}
