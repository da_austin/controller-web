package com.sbdinc.da.smartdoor.dcweb.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entity class of table DMT_MAS_EQUIP_STORE_DIM
 *
 * @author Dian Jiao
 *
 */

@Entity
@Table(name = "DMT_MAS_EQUIP_STORE_DIM")
public class EquipStoreDim implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name = "EQUIP_STORE_ID")
    private int id;
	
	@Column(name = "SRC_SYS_CD")
	private String srcSysCd;
	
	@Column(name = "EQUIP_STORE_SSK")
	private String equipStoreSsk;
	
	@Column(name = "EQUIP_SSK")
	private String equipSsk;
	
	@Column(name = "SHIPTO_CUST_SSK")
	private String shiptoCustSsk;
	
	@Column(name = "SHIPTO_CUST_ID")
	private int shiptoCustId;
	
	@Column(name = "EQUIP_STORE_CD")
	private String equipStoreCd;
	
	@Column(name = "EQUIP_NBR")
	private String equipNbr;
	
	@Column(name = "EQUIP_DESC")
	private String equipDesc;
	
	@Column(name = "SHIPTO_CD")
	private String shiptoCd;
	
	@Column(name = "SHIPTO_DESC")
	private String shiptoDesc;
	
	@Column(name = "EQUIP_SHIPTO_ADDR_1")
	private String equipShiptoAddr1;
	
	@Column(name = "EQUIP_SHIPTO_ADDR_2")
	private String equipShiptoAddr2;
	
	@Column(name = "EQUIP_SHIPTO_ADDR_3")
	private String equipShiptoAddr3;
	
	@Column(name = "EQUIP_SHIPTO_ADDR_4")
	private String equipShiptoAddr4;
	
	@Column(name = "EQUIP_SHIPTO_CITY")
	private String equipShiptoCity;
	
	@Column(name = "EQUIP_SHIPTO_STATE_CD")
	private String equipShiptoStateCd;
	
	@Column(name = "EQUIP_SHIPTO_PSTL_CD")
	private String equipShiptoPstlCd;
	
	@Column(name = "EQUIP_SHIPTO_CNTRY_CD")
	private String equipShiptoCntryCd;
	
	@Column(name = "TECH_NBR")
	private String techNbr;
	
	@Column(name = "BRNCH_ID")
	private int brnchId;
	
	@Column(name = "SVC_BRNCH_SSK")
	private String svcBrnchSsk;
	
	@Column(name = "SVC_BRNCH_CD")
	private String svcBrnchCd;
	
	@Column(name = "EQUIP_TYP_CD")
	private String equipTypCd;
	
	@Column(name = "EQUIP_TYP_DESC")
	private String equipTypDesc;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "BEGIN_WRNTY_DTE")
	private Date beginWrntyDte;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "END_WRNTY_DTE")
	private Date ENDWrntyDte;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FIRST_SVC_DTM")
	private Date firstSvcDtm;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "INSTALL_DTM")
	private Date installDtm;
	
	@Column(name = "ETL_JOB_INVK")
	private String etlJobInvk;
	
	@Column(name = "ETL_JOB_NAME")
	private String etlJobName;
	
	@Column(name = "ETL_BATCH_ID")
	private int etlBatchId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ETL_CREATE_DTM")
	private Date etlCreateDtm;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ETL_LOD_DTM")
	private Date etlLodDtm;
	
	@Column(name = "ETL_MSNG_FK_FLAG")
	private char etlMsngFkFlag;
	
	@Column(name = "ACTV_STAT_FLAG")
	private char actvStatFlag;
	
	@Column(name = "DEL_FLAG")
	private char delFlag;
	
	@Column(name = "SLS_ORD_NBR")
	private String slsOrdNbr;
	
	@Column(name = "SLS_ORD_DTL_NBR")
	private String slsOrdDtlNbr;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSrcSysCd() {
		return srcSysCd;
	}

	public void setSrcSysCd(String srcSysCd) {
		this.srcSysCd = srcSysCd;
	}

	public String getEquipStoreSsk() {
		return equipStoreSsk;
	}

	public void setEquipStoreSsk(String equipStoreSsk) {
		this.equipStoreSsk = equipStoreSsk;
	}

	public String getEquipSsk() {
		return equipSsk;
	}

	public void setEquipSsk(String equipSsk) {
		this.equipSsk = equipSsk;
	}

	public String getShiptoCustSsk() {
		return shiptoCustSsk;
	}

	public void setShiptoCustSsk(String shiptoCustSsk) {
		this.shiptoCustSsk = shiptoCustSsk;
	}

	public int getShiptoCustId() {
		return shiptoCustId;
	}

	public void setShiptoCustId(int shiptoCustId) {
		this.shiptoCustId = shiptoCustId;
	}

	public String getEquipStoreCd() {
		return equipStoreCd;
	}

	public void setEquipStoreCd(String equipStoreCd) {
		this.equipStoreCd = equipStoreCd;
	}

	public String getEquipNbr() {
		return equipNbr;
	}

	public void setEquipNbr(String equipNbr) {
		this.equipNbr = equipNbr;
	}

	public String getEquipDesc() {
		return equipDesc;
	}

	public void setEquipDesc(String equipDesc) {
		this.equipDesc = equipDesc;
	}

	public String getShiptoCd() {
		return shiptoCd;
	}

	public void setShiptoCd(String shiptoCd) {
		this.shiptoCd = shiptoCd;
	}

	public String getShiptoDesc() {
		return shiptoDesc;
	}

	public void setShiptoDesc(String shiptoDesc) {
		this.shiptoDesc = shiptoDesc;
	}

	public String getEquipShiptoAddr1() {
		return equipShiptoAddr1;
	}

	public void setEquipShiptoAddr1(String equipShiptoAddr1) {
		this.equipShiptoAddr1 = equipShiptoAddr1;
	}

	public String getEquipShiptoAddr2() {
		return equipShiptoAddr2;
	}

	public void setEquipShiptoAddr2(String equipShiptoAddr2) {
		this.equipShiptoAddr2 = equipShiptoAddr2;
	}

	public String getEquipShiptoAddr3() {
		return equipShiptoAddr3;
	}

	public void setEquipShiptoAddr3(String equipShiptoAddr3) {
		this.equipShiptoAddr3 = equipShiptoAddr3;
	}

	public String getEquipShiptoAddr4() {
		return equipShiptoAddr4;
	}

	public void setEquipShiptoAddr4(String equipShiptoAddr4) {
		this.equipShiptoAddr4 = equipShiptoAddr4;
	}

	public String getEquipShiptoCity() {
		return equipShiptoCity;
	}

	public void setEquipShiptoCity(String equipShiptoCity) {
		this.equipShiptoCity = equipShiptoCity;
	}

	public String getEquipShiptoStateCd() {
		return equipShiptoStateCd;
	}

	public void setEquipShiptoStateCd(String equipShiptoStateCd) {
		this.equipShiptoStateCd = equipShiptoStateCd;
	}

	public String getEquipShiptoPstlCd() {
		return equipShiptoPstlCd;
	}

	public void setEquipShiptoPstlCd(String equipShiptoPstlCd) {
		this.equipShiptoPstlCd = equipShiptoPstlCd;
	}

	public String getEquipShiptoCntryCd() {
		return equipShiptoCntryCd;
	}

	public void setEquipShiptoCntryCd(String equipShiptoCntryCd) {
		this.equipShiptoCntryCd = equipShiptoCntryCd;
	}

	public String getTechNbr() {
		return techNbr;
	}

	public void setTechNbr(String techNbr) {
		this.techNbr = techNbr;
	}

	public int getBrnchId() {
		return brnchId;
	}

	public void setBrnchId(int brnchId) {
		this.brnchId = brnchId;
	}

	public String getSvcBrnchSsk() {
		return svcBrnchSsk;
	}

	public void setSvcBrnchSsk(String svcBrnchSsk) {
		this.svcBrnchSsk = svcBrnchSsk;
	}

	public String getSvcBrnchCd() {
		return svcBrnchCd;
	}

	public void setSvcBrnchCd(String svcBrnchCd) {
		this.svcBrnchCd = svcBrnchCd;
	}

	public String getEquipTypCd() {
		return equipTypCd;
	}

	public void setEquipTypCd(String equipTypCd) {
		this.equipTypCd = equipTypCd;
	}

	public String getEquipTypDesc() {
		return equipTypDesc;
	}

	public void setEquipTypDesc(String equipTypDesc) {
		this.equipTypDesc = equipTypDesc;
	}

	public Date getBeginWrntyDte() {
		return beginWrntyDte;
	}

	public void setBeginWrntyDte(Date beginWrntyDte) {
		this.beginWrntyDte = beginWrntyDte;
	}

	public Date getENDWrntyDte() {
		return ENDWrntyDte;
	}

	public void setENDWrntyDte(Date eNDWrntyDte) {
		ENDWrntyDte = eNDWrntyDte;
	}

	public Date getFirstSvcDtm() {
		return firstSvcDtm;
	}

	public void setFirstSvcDtm(Date firstSvcDtm) {
		this.firstSvcDtm = firstSvcDtm;
	}

	public Date getInstallDtm() {
		return installDtm;
	}

	public void setInstallDtm(Date installDtm) {
		this.installDtm = installDtm;
	}

	public String getEtlJobInvk() {
		return etlJobInvk;
	}

	public void setEtlJobInvk(String etlJobInvk) {
		this.etlJobInvk = etlJobInvk;
	}

	public String getEtlJobName() {
		return etlJobName;
	}

	public void setEtlJobName(String etlJobName) {
		this.etlJobName = etlJobName;
	}

	public int getEtlBatchId() {
		return etlBatchId;
	}

	public void setEtlBatchId(int etlBatchId) {
		this.etlBatchId = etlBatchId;
	}

	public Date getEtlCreateDtm() {
		return etlCreateDtm;
	}

	public void setEtlCreateDtm(Date etlCreateDtm) {
		this.etlCreateDtm = etlCreateDtm;
	}

	public Date getEtlLodDtm() {
		return etlLodDtm;
	}

	public void setEtlLodDtm(Date etlLodDtm) {
		this.etlLodDtm = etlLodDtm;
	}

	public char getEtlMsngFkFlag() {
		return etlMsngFkFlag;
	}

	public void setEtlMsngFkFlag(char etlMsngFkFlag) {
		this.etlMsngFkFlag = etlMsngFkFlag;
	}

	public char getActvStatFlag() {
		return actvStatFlag;
	}

	public void setActvStatFlag(char actvStatFlag) {
		this.actvStatFlag = actvStatFlag;
	}

	public char getDelFlag() {
		return delFlag;
	}

	public void setDelFlag(char delFlag) {
		this.delFlag = delFlag;
	}

	public String getSlsOrdNbr() {
		return slsOrdNbr;
	}

	public void setSlsOrdNbr(String slsOrdNbr) {
		this.slsOrdNbr = slsOrdNbr;
	}

	public String getSlsOrdDtlNbr() {
		return slsOrdDtlNbr;
	}

	public void setSlsOrdDtlNbr(String slsOrdDtlNbr) {
		this.slsOrdDtlNbr = slsOrdDtlNbr;
	}
	
	
}
