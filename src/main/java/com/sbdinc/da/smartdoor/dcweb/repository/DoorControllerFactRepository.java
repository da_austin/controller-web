package com.sbdinc.da.smartdoor.dcweb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sbdinc.da.smartdoor.dcweb.model.DoorControllerFact;

/**
 * @author Dian Jiao
 *
 */
@Repository
public interface DoorControllerFactRepository extends JpaRepository<DoorControllerFact, Integer> {
	
	public DoorControllerFact findFirstByDoorIdOrderByInsertTsDesc(String doorId);

}
