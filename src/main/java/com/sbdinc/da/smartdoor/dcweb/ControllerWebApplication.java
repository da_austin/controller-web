package com.sbdinc.da.smartdoor.dcweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class ControllerWebApplication extends SpringBootServletInitializer {

	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(ControllerWebApplication.class);
    }
	
	public static void main(String[] args) {
		SpringApplication.run(ControllerWebApplication.class, args);
	}
}
