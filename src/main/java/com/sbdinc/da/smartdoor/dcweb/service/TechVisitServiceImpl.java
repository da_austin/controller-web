package com.sbdinc.da.smartdoor.dcweb.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sbdinc.da.smartdoor.dcweb.model.TechVisit;
import com.sbdinc.da.smartdoor.dcweb.repository.TechVisitRepository;

/**
 * 
 * Implementation class for TechVisit Service
 *
 * @author Dian Jiao
 *
 */
@Service
public class TechVisitServiceImpl implements TechVisitService {

	@Autowired
	TechVisitRepository techVisitRepository;
	
	@Override
	public long saveTechVisit(TechVisit techVisit) {
		Date dt = new Date();
		techVisit.setInsertTS(dt);
		techVisitRepository.saveAndFlush(techVisit);
		return techVisit.getId();
	}
}
