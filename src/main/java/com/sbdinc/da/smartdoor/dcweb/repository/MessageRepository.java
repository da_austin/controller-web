package com.sbdinc.da.smartdoor.dcweb.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.sbdinc.da.smartdoor.dcweb.model.Message;

/**
 * @author Dian Jiao
 *
 */
public interface MessageRepository extends JpaRepository<Message, Long> {
	@Modifying(clearAutomatically = true)
	@Transactional
	@Query("UPDATE Message m set m.loadStatus = :code, m.note = :note WHERE m.id = :msgId")
	int updateLoadStatus(@Param("msgId") long msgId, @Param("code") char code, @Param("note") String note);
}
