package com.sbdinc.da.smartdoor.dcweb.service;

import com.sbdinc.da.smartdoor.dcweb.model.TechVisit;

/**
 * 
 * Interface of TechVist Service
 *
 * @author Dian Jiao
 *
 */
public interface TechVisitService {

	long saveTechVisit(TechVisit techVisit);
}
