package com.sbdinc.da.smartdoor.dcweb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sbdinc.da.smartdoor.dcweb.model.EquipStoreDim;

/**
 *
 *
 * @author Dian Jiao
 *
 */
public interface EquipStoreDimRepository extends JpaRepository<EquipStoreDim, Integer> {

}
