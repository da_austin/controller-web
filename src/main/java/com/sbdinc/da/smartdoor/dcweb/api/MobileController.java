package com.sbdinc.da.smartdoor.dcweb.api;

import org.springframework.http.HttpStatus;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.sbdinc.da.smartdoor.dcweb.model.DoorControllerFact;
import com.sbdinc.da.smartdoor.dcweb.model.MobileVO;
import com.sbdinc.da.smartdoor.dcweb.model.TechVisit;
import com.sbdinc.da.smartdoor.dcweb.service.BasicSettingService;
import com.sbdinc.da.smartdoor.dcweb.service.DoorControllerFactService;
import com.sbdinc.da.smartdoor.dcweb.service.ErrorHistoryService;
import com.sbdinc.da.smartdoor.dcweb.service.MessageService;
import com.sbdinc.da.smartdoor.dcweb.service.MobileVOService;
import com.sbdinc.da.smartdoor.dcweb.service.SettingService;
import com.sbdinc.da.smartdoor.dcweb.service.TechVisitService;

import io.swagger.annotations.ApiOperation;

/**
 * 
 * Controller class with endpoints handlers
 * 
 * @author Dian Jiao
 *
 */
@RestController
@CrossOrigin
@RequestMapping("/mobile")
public class MobileController {
	private static final Logger logger = LoggerFactory.getLogger(MobileController.class);
	
	// Constant variables for message type
	private static final String TYPE_CONTROLLER = "controller";
	private static final String TYPE_ERROR = "error history";
	private static final String TYPE_SETTING = "setting history";
	private static final String TYPE_BASIC_SETTING = "basic setting";
	
	// Constant variables for message format
	private static final String FMT_JSON = "json";
	private static final String FMT_XML = "xml";
	
	@Autowired
	MobileVOService mobileVOService;
	
	@Autowired
	MessageService messageService;
	
	@Autowired
	TechVisitService techVisitService;
	
	@Autowired
	DoorControllerFactService doorControllerFactService;
	
	@Autowired
	ErrorHistoryService errorHistoryService;
	
	@Autowired
	SettingService settingService;
	
	@Autowired
	BasicSettingService basicSettingService;
	
	@ApiOperation(value = "Greeting", response = String.class)
	@RequestMapping(
			value = "/greeting/",
			method = RequestMethod.GET,
			produces = "text/plain")
	public String helloWorld(@RequestParam(value="name", required=false, defaultValue="World") String name) {
		return "Hello " + name + "!";
	}
	
	@ApiOperation(value = "Send controller data", response = Long.class)
	@RequestMapping(
		value = "/controller/",
		method = RequestMethod.POST,
		consumes = "application/json",
		produces = "text/plain")
	@ResponseStatus(HttpStatus.CREATED)
	public String persistController(
			@RequestParam(value="pointOfTrigger", required=false, defaultValue="NA") String trigger,
			@Valid @RequestBody MobileVO vo
			) {
		if (trigger == null) trigger = "NA";
		long visitId = 0;
		String androidId = vo.getAndroidId();	
		String source = androidId.contains("CIU") ? "wifi" : "mobile";
		long msgId = messageService.saveMessage(source, TYPE_CONTROLLER, FMT_JSON, trigger, vo.toString());
		if (vo.getActiveDoorId().trim() == "") {
			messageService.updateMessage(msgId, 'F', "ActiveDoorId is empty or null");
			logger.error("ActiveDoorId is empty or null");
		}
				
		//Persist Technician Visit data only if androidID does not have CIU (means from WIFI)
		if (!androidId.contains("CIU")) {
			TechVisit techVisit = mobileVOService.voToTechVisit(vo);
			techVisit.setMessageId(msgId);
			visitId = techVisitService.saveTechVisit(techVisit);
			if (visitId == 0) {
				messageService.updateMessage(msgId, 'F', "TechVisit persistence failed.");
				logger.error("TechVisit persistence failed");
			}
				
			else {
				messageService.updateMessage(msgId, 'P', "");
				logger.info("TechVisit record inserted");
			}
		}
		
		
		// Persist door controller fact data
		DoorControllerFact doorControllerFact = mobileVOService.voToDoorControllerFact(vo);
		doorControllerFact.setMessageId(msgId);
		doorControllerFact.setVisitId(visitId);
		long dcfId = doorControllerFactService.saveControllerDetail(doorControllerFact);
		if (dcfId == 0) {
			messageService.updateMessage(msgId, 'F', "DoorControllerFact persistence failed.");
			logger.error("DoorControllerFact persistence failed");
		}
		else {
			messageService.updateMessage(msgId, 'P', "");
			logger.info("DoorControllerFact record inserted");
		}
		return Long.toString(visitId);
	}
	
	
	@ApiOperation(value = "Send controller error history data")
	@RequestMapping(
			value = "/err",
			method = RequestMethod.POST,
			consumes = "application/xml",
			produces = "text/plain")
	@ResponseStatus(HttpStatus.CREATED)
	public String persistError(
			@RequestParam(value="visit", required=false, defaultValue="0") Integer visitId, 
			@RequestParam(value="pointOfTrigger", required=false, defaultValue="NA") String trigger,
			@RequestBody String errXML
			) {
		if (trigger == null) trigger = "NA";
		String source = visitId == 0 ? "wifi" : "mobile";
		long msgId = messageService.saveMessage(source, TYPE_ERROR, FMT_XML, trigger, errXML);
		int errCnt = -1;
		String errMsg = null;
		try {
			errCnt = errorHistoryService.saveErrorHistory(errXML, msgId, visitId);
		} catch (Exception e) {
			errMsg = e.toString();
			if (errMsg.length() >= 200) 
				errMsg = errMsg.substring(0, 199);
			logger.error(errMsg);
		}
		if (errCnt == -1) {
			messageService.updateMessage(msgId, 'F', errMsg);
			logger.error("ErrorHist persistence failed");
		}
		else if (errCnt == 0) {
			messageService.updateMessage(msgId, 'F', "No error found in XML");
			logger.error("ErrorHist persistence failed");
		}
		else {
			messageService.updateMessage(msgId, 'P', "");
			logger.info("ErrorHist record inserted");
		}
		return Integer.toString(errCnt);
	}
	
	@ApiOperation(value = "Send controller basic setting data")
	@RequestMapping(
			value = "/basicsetting",
			method = RequestMethod.POST,
			consumes = "application/xml",
			produces = "text/plain")
	@ResponseStatus(HttpStatus.CREATED)
	public String persistSetting(
			@RequestParam(value="visit", required=false, defaultValue="0") Integer visitId, 
			@RequestParam(value="pointOfTrigger", required=false, defaultValue="NA") String trigger,
			@RequestBody String errXML
			) {
		if (trigger == null) trigger = "NA";
		String source = visitId == 0 ? "wifi" : "mobile";
		String errMsg = null;
		long msgId = messageService.saveMessage(source, TYPE_BASIC_SETTING, FMT_XML, trigger, errXML);
		int basicSettingId = 0;
		try {
			basicSettingId = basicSettingService.saveBasicSetting(errXML, msgId, visitId);
		} catch (Exception e) {
			errMsg = e.toString();
			if (errMsg.length() >= 200) 
				errMsg = errMsg.substring(0, 199);
			logger.error(errMsg);
		}
		if (basicSettingId == 0) {
			messageService.updateMessage(msgId, 'F', errMsg);
			logger.error("Basic Setting XML persistence failed");
		}
		else {
			messageService.updateMessage(msgId, 'P', "");
			logger.info("Basic Setting record inserted");
		}
		return Integer.toString(basicSettingId);
	}
	
	@ApiOperation(value = "Send controller setting parameters data")
	@RequestMapping(
			value = "/setting",
			method = RequestMethod.POST,
			consumes = "application/xml",
			produces = "text/plain")
	@ResponseStatus(HttpStatus.CREATED)
	public String persistBasicSetting(
			@RequestParam(value="visit", required=false, defaultValue="0") Integer visitId, 
			@RequestParam(value="pointOfTrigger", required=false, defaultValue="NA") String trigger,
			@RequestBody String errXML
			) {
		if (trigger == null) trigger = "NA";
		String source = visitId == 0 ? "wifi" : "mobile";
		long msgId = messageService.saveMessage(source, TYPE_SETTING, FMT_XML, trigger, errXML);
		long settingId = 0;
		String errMsg = null;
		try {
			settingId = settingService.saveSetting(errXML, msgId, visitId);
		} catch (Exception e) {
			errMsg = e.toString();
			if (errMsg.length() >= 200) 
				errMsg = errMsg.substring(0, 199);
			logger.error(errMsg);
		}
		if (settingId == 0) {
			messageService.updateMessage(msgId, 'F', errMsg);
			logger.error("Setting XML persistence failed");
		}
		else {
			messageService.updateMessage(msgId, 'P', "");
			logger.info("Setting record inserted");
		}
		return Long.toString(settingId);
	}
	

}
