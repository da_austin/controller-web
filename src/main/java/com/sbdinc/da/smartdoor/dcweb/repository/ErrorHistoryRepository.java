package com.sbdinc.da.smartdoor.dcweb.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.sbdinc.da.smartdoor.dcweb.model.ErrorHistory;
/**
 * @author Dian Jiao
 *
 */
public interface ErrorHistoryRepository extends JpaRepository<ErrorHistory, Integer> {
	
	@Query("SELECT e from ErrorHistory e WHERE e.doorId=?1")
	public List<ErrorHistory> findByDoorId(String doorId);
	
//	@Query("SELECT e from ErrorHistory e order by e.insertTS desc limit 100")
//	public List<ErrorHistory> findAll();
	
	public List<ErrorHistory> findTop100ByOrderByInsertTSDesc();

}
