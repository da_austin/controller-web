package com.sbdinc.da.smartdoor.dcweb.service;

import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sbdinc.da.smartdoor.dcweb.model.BasicSetting;
import com.sbdinc.da.smartdoor.dcweb.repository.BasicSettingRepository;
import com.sbdinc.da.smartdoor.dcweb.util.XMLParser;

/**
 *
 *
 * @author Dian Jiao
 *
 */
@Service
public class BasicSettingServiceImpl implements BasicSettingService {
	private static final Logger logger = LoggerFactory.getLogger(BasicSettingServiceImpl.class);

	final private SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	
	@Autowired
	BasicSettingRepository basicSettingRepository;
	
	@Autowired
	DoorControllerFactService doorControllerFactService;
	
	@Override
	public int saveBasicSetting(String xml, long msgId, long visitId) throws ParserConfigurationException, SAXException, IOException{
		return parseBasicSettingXML(xml, msgId, visitId);
	}
	
	/**
	 * Parse settings xml
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
	 * @throws ParseException 
	 */
	private int parseBasicSettingXML(String xml, long msgId, long visitId) throws ParserConfigurationException, SAXException, IOException{
		int basicSettingId = 0;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    InputSource is = new InputSource(new StringReader(xml));
	    Document doc = builder.parse(is);
	    doc.getDocumentElement().normalize();
	    Element root = doc.getDocumentElement();
	    NodeList children = root.getChildNodes();
	    
	    // parse header
	    String doorId = XMLParser.getNodeValue("activeDoorID", children);
	    long controllerId = doorControllerFactService.findIDbyDoorId(doorId);
	    
	    // Check whether the matching controller exists
	    if (controllerId == 0) {
    		logger.error("No matching controller exists in ControllerDoor table with doorId (" + doorId + ")");
	    	return 0;
	    }
	    String serialNumber = XMLParser.getNodeValue("controllerSerialNumber", children);
	    if (serialNumber.length() > 40) serialNumber = serialNumber.substring(0, 40);
	    String date = XMLParser.getNodeValue("date", children);
	    String opCode = XMLParser.getNodeValue("op_mode", children);
	    
	    String handing1 = XMLParser.getNodeValue("handing1", children);
	    String handing2 = XMLParser.getNodeValue("handing2", children);
	    
	    // parse param nodes
	    List<Node> params = XMLParser.getNodes("param", children);
	    
	    BasicSetting basicSetting = new BasicSetting();
	    basicSetting.setDoorId(doorId);
	    basicSetting.setControllerSerialNumber(serialNumber);
	    basicSetting.setMessageId(msgId);
	    basicSetting.setVisitId(visitId);
	    if (!StringUtils.isEmpty(date))
	    	try {
	    		basicSetting.setReadDt(FORMATTER.parse(date));
	    	}
	    	catch (ParseException e) {
	    		logger.warn("ParseException ReadDt");
	    	}
	    if (isInteger(opCode))
	    	basicSetting.setOpCode(Integer.parseInt(opCode));
	    Date insertTs = new Date();
	    basicSetting.setInsertTs(insertTs);
	    if (isInteger(handing1))
	    	basicSetting.setHanding1(Integer.parseInt(handing1));
	    if (isInteger(handing2))
	    	basicSetting.setHanding2(Integer.parseInt(handing2));
	    
	    for (Node param : params) {
	    	NodeList seeds = param.getChildNodes();
	    	String name = XMLParser.getNodeValue("name", seeds);
	    	if (name == "") continue;
	    	
	    	if (name.equalsIgnoreCase("safety logic")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1))
	    			basicSetting.setSafetyLogic(Integer.parseInt(value1));
	    		
	    	}	
	    	if (name.equalsIgnoreCase("hold beam type")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1))
	    			basicSetting.setHoldBeamType(Integer.parseInt(value1));	
	    	}	
	    	if (name.equalsIgnoreCase("function switch type")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1))
	    			basicSetting.setFunctionSwitchType(Integer.parseInt(value1));	
	    	}	

	    }
	    
	    //persist
	    basicSettingRepository.save(basicSetting);
	    basicSettingId = basicSetting.getId();

		return basicSettingId;
	}

	private boolean isInteger(String s) {
	    try
	    {
	        Integer.parseInt(s);
	        return true;
	    } catch (NumberFormatException ex)
	    {
	        return false;
	    }
	}

}
