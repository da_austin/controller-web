package com.sbdinc.da.smartdoor.dcweb.service;

import java.io.IOException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import com.sbdinc.da.smartdoor.dcweb.model.ErrorHistory;

/**
 * @author Dian Jiao
 *
 */
public interface ErrorHistoryService {
	int saveErrorHistory(String xml, long msgId, long visitId) throws ParserConfigurationException, SAXException, IOException;
	
	List<ErrorHistory> findAllErrors();
	List<ErrorHistory> findErrorsByDoorId(String doorId);


}
