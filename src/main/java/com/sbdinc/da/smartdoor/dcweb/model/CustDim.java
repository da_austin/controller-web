package com.sbdinc.da.smartdoor.dcweb.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity class for table DMT_MAS_CUST_DIM
 *
 * @author Dian Jiao
 *
 */

@Entity
@Table(name="dmt_mas_cust_dim")
public class CustDim implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="cust_id")
	private int custId;

	@Column(name="addr_ln_1")
	private String addrLn1;

	@Column(name="addr_ln_2")
	private String addrLn2;

	@Column(name="addr_ln_3")
	private String addrLn3;

	@Column(name="addr_ln_4")
	private String addrLn4;

	@Column(name="affil_flag")
	private String affilFlag;

	@Column(name="ar_common_cust_name")
	private String arCommonCustName;

	@Column(name="ar_cust_credit_ctrl_area")
	private String arCustCreditCtrlArea;

	@Column(name="ar_cust_id")
	private int arCustId;

	@Column(name="ar_cust_nbr")
	private String arCustNbr;

	@Column(name="ar_cust_src_sys_cd")
	private String arCustSrcSysCd;

	@Column(name="ar_cust_ssk")
	private String arCustSsk;

	@Column(name="atm_cust_cd")
	private String atmCustCd;

	@Column(name="atm_cust_id")
	private int atmCustId;

	@Column(name="atm_cust_ssk")
	private String atmCustSsk;

	@Column(name="atm_src_sys_cd")
	private String atmSrcSysCd;

	@Column(name="atm_src_sys_id")
	private int atmSrcSysId;

	@Column(name="billto_flag")
	private String billtoFlag;

	@Column(name="city_name")
	private String cityName;

	@Column(name="cntry_cd")
	private String cntryCd;

	@Column(name="cntry_name")
	private String cntryName;

	@Column(name="county_name")
	private String countyName;

	@Column(name="crncy_cd")
	private String crncyCd;

	@Column(name="cust_lvl_2_name")
	private String custLvl2Name;

	@Column(name="cust_lvl_2_nbr")
	private String custLvl2Nbr;

	@Column(name="cust_lvl_3_name")
	private String custLvl3Name;

	@Column(name="cust_lvl_3_nbr")
	private String custLvl3Nbr;

	@Column(name="cust_lvl_4_name")
	private String custLvl4Name;

	@Column(name="cust_lvl_4_nbr")
	private String custLvl4Nbr;

	@Column(name="cust_lvl_5_name")
	private String custLvl5Name;

	@Column(name="cust_lvl_5_nbr")
	private String custLvl5Nbr;

	@Column(name="cust_lvl_6_name")
	private String custLvl6Name;

	@Column(name="cust_lvl_6_nbr")
	private String custLvl6Nbr;

	@Column(name="cust_lvl_7_name")
	private String custLvl7Name;

	@Column(name="cust_lvl_7_nbr")
	private String custLvl7Nbr;

	@Column(name="cust_lvl_8_name")
	private String custLvl8Name;

	@Column(name="cust_lvl_8_nbr")
	private String custLvl8Nbr;

	@Column(name="cust_name")
	private String custName;

	@Column(name="cust_nbr")
	private String custNbr;

	@Column(name="cust_srch_text")
	private String custSrchText;

	@Column(name="cust_stat_cd")
	private String custStatCd;

	@Column(name="cust_stat_desc")
	private String custStatDesc;

	@Column(name="cust_typ_cd")
	private String custTypCd;

	@Column(name="cust_typ_desc")
	private String custTypDesc;

	@Column(name="dflt_prod_src_fac_cd")
	private String dfltProdSrcFacCd;

	@Column(name="dflt_prod_src_fac_desc")
	private String dfltProdSrcFacDesc;

	@Column(name="duns_nbr")
	private String dunsNbr;

	@Column(name="end_user_flag")
	private String endUserFlag;

	@Column(name="etl_batch_id")
	private int etlBatchId;

	@Column(name="etl_create_dtm")
	private Date etlCreateDtm;

	@Column(name="etl_job_invk")
	private String etlJobInvk;

	@Column(name="etl_job_name")
	private String etlJobName;

	@Column(name="etl_load_dtm")
	private Date etlLoadDtm;

	@Column(name="etl_msng_fk_flag")
	private String etlMsngFkFlag;

	@Column(name="fax_nbr")
	private String faxNbr;

	@Column(name="hier_exception_cd")
	private String hierExceptionCd;

	@Column(name="hier_exception_flag")
	private String hierExceptionFlag;

	@Column(name="hier_typ_cd")
	private String hierTypCd;

	@Column(name="hrmz_cust_name")
	private String hrmzCustName;

	@Column(name="hrmz_cust_nbr")
	private String hrmzCustNbr;

	@Column(name="inco_terms_1_cd")
	private String incoTerms1Cd;

	@Column(name="inco_terms_1_desc")
	private String incoTerms1Desc;

	@Column(name="new_acct_dte")
	private Date newAcctDte;

	@Column(name="payer_flag")
	private String payerFlag;

	@Column(name="phn_nbr")
	private String phnNbr;

	@Column(name="prim_slsrep_cd")
	private String primSlsrepCd;

	@Column(name="prim_slsrep_name")
	private String primSlsrepName;

	@Column(name="prim_slsrep_userid")
	private String primSlsrepUserid;

	@Column(name="prtcl_cust_extnl_nte_amt")
	private BigDecimal prtclCustExtnlNteAmt;

	@Column(name="prtcl_cust_itrl_nte_amt")
	private BigDecimal prtclCustItrlNteAmt;

	@Column(name="prtcl_cust_name")
	private String prtclCustName;

	@Column(name="prtcl_cust_nbr")
	private String prtclCustNbr;

	@Column(name="prtcl_cust_zone_chrg_amt")
	private BigDecimal prtclCustZoneChrgAmt;

	@Column(name="pstl_cd")
	private String pstlCd;

	@Column(name="pymt_terms_cd")
	private String pymtTermsCd;

	@Column(name="pymt_terms_desc")
	private String pymtTermsDesc;

	@Column(name="sap_acct_asgn_grp_cd")
	private String sapAcctAsgnGrpCd;

	@Column(name="sap_acct_asgn_grp_desc")
	private String sapAcctAsgnGrpDesc;

	@Column(name="sap_cust_acct_grp_cd")
	private String sapCustAcctGrpCd;

	@Column(name="sap_cust_acct_grp_desc")
	private String sapCustAcctGrpDesc;

	@Column(name="sap_cust_cls_cd")
	private String sapCustClsCd;

	@Column(name="sap_cust_cls_desc")
	private String sapCustClsDesc;

	@Column(name="sap_cust_grp_cd")
	private String sapCustGrpCd;

	@Column(name="sap_cust_grp_desc")
	private String sapCustGrpDesc;

	@Column(name="sap_cust_grp3_cd")
	private String sapCustGrp3Cd;

	@Column(name="sap_cust_grp3_desc")
	private String sapCustGrp3Desc;

	@Column(name="sap_dist_chnl_cd")
	private String sapDistChnlCd;

	@Column(name="sap_dist_chnl_name")
	private String sapDistChnlName;

	@Column(name="sap_div_cd")
	private String sapDivCd;

	@Column(name="sap_div_name")
	private String sapDivName;

	@Column(name="sap_sls_grp_cd")
	private String sapSlsGrpCd;

	@Column(name="sap_sls_grp_desc")
	private String sapSlsGrpDesc;

	@Column(name="sap_sls_office_cd")
	private String sapSlsOfficeCd;

	@Column(name="sap_sls_office_desc")
	private String sapSlsOfficeDesc;

	@Column(name="sap_sls_org_cd")
	private String sapSlsOrgCd;

	@Column(name="sap_sls_org_name")
	private String sapSlsOrgName;

	@Column(name="sat_cust_typ_chnl_cd")
	private String satCustTypChnlCd;

	@Column(name="sat_cust_typ_chnl_desc")
	private String satCustTypChnlDesc;

	@Column(name="scndry_slsrep_cd")
	private String scndrySlsrepCd;

	@Column(name="scndry_slsrep_name")
	private String scndrySlsrepName;

	@Column(name="scndry_slsrep_userid")
	private String scndrySlsrepUserid;

	@Column(name="sdw_cust_ssk")
	private String sdwCustSsk;

	@Column(name="shipto_flag")
	private String shiptoFlag;

	@Column(name="soldto_flag")
	private String soldtoFlag;

	@Column(name="src_sys_cd")
	private String srcSysCd;

	@Column(name="src_sys_del_flag")
	private String srcSysDelFlag;

	@Column(name="state_cd")
	private String stateCd;

	@Column(name="state_name")
	private String stateName;

	@Column(name="tax_id_nbr")
	private String taxIdNbr;

	@Column(name="terms_net_dys")
	private int termsNetDys;

	@Column(name="ultm_prnt_cust_id")
	private int ultmPrntCustId;

	@Column(name="ultm_prnt_cust_name")
	private String ultmPrntCustName;

	@Column(name="ultm_prnt_cust_nbr")
	private String ultmPrntCustNbr;

	public int getCustId() {
		return this.custId;
	}

	public void setCustId(int custId) {
		this.custId = custId;
	}

	public String getAddrLn1() {
		return this.addrLn1;
	}

	public void setAddrLn1(String addrLn1) {
		this.addrLn1 = addrLn1;
	}

	public String getAddrLn2() {
		return this.addrLn2;
	}

	public void setAddrLn2(String addrLn2) {
		this.addrLn2 = addrLn2;
	}

	public String getAddrLn3() {
		return this.addrLn3;
	}

	public void setAddrLn3(String addrLn3) {
		this.addrLn3 = addrLn3;
	}

	public String getAddrLn4() {
		return this.addrLn4;
	}

	public void setAddrLn4(String addrLn4) {
		this.addrLn4 = addrLn4;
	}

	public String getAffilFlag() {
		return this.affilFlag;
	}

	public void setAffilFlag(String affilFlag) {
		this.affilFlag = affilFlag;
	}

	public String getArCommonCustName() {
		return this.arCommonCustName;
	}

	public void setArCommonCustName(String arCommonCustName) {
		this.arCommonCustName = arCommonCustName;
	}

	public String getArCustCreditCtrlArea() {
		return this.arCustCreditCtrlArea;
	}

	public void setArCustCreditCtrlArea(String arCustCreditCtrlArea) {
		this.arCustCreditCtrlArea = arCustCreditCtrlArea;
	}

	public int getArCustId() {
		return this.arCustId;
	}

	public void setArCustId(int arCustId) {
		this.arCustId = arCustId;
	}

	public String getArCustNbr() {
		return this.arCustNbr;
	}

	public void setArCustNbr(String arCustNbr) {
		this.arCustNbr = arCustNbr;
	}

	public String getArCustSrcSysCd() {
		return this.arCustSrcSysCd;
	}

	public void setArCustSrcSysCd(String arCustSrcSysCd) {
		this.arCustSrcSysCd = arCustSrcSysCd;
	}

	public String getArCustSsk() {
		return this.arCustSsk;
	}

	public void setArCustSsk(String arCustSsk) {
		this.arCustSsk = arCustSsk;
	}

	public String getAtmCustCd() {
		return this.atmCustCd;
	}

	public void setAtmCustCd(String atmCustCd) {
		this.atmCustCd = atmCustCd;
	}

	public int getAtmCustId() {
		return this.atmCustId;
	}

	public void setAtmCustId(int atmCustId) {
		this.atmCustId = atmCustId;
	}

	public String getAtmCustSsk() {
		return this.atmCustSsk;
	}

	public void setAtmCustSsk(String atmCustSsk) {
		this.atmCustSsk = atmCustSsk;
	}

	public String getAtmSrcSysCd() {
		return this.atmSrcSysCd;
	}

	public void setAtmSrcSysCd(String atmSrcSysCd) {
		this.atmSrcSysCd = atmSrcSysCd;
	}

	public int getAtmSrcSysId() {
		return this.atmSrcSysId;
	}

	public void setAtmSrcSysId(int atmSrcSysId) {
		this.atmSrcSysId = atmSrcSysId;
	}

	public String getBilltoFlag() {
		return this.billtoFlag;
	}

	public void setBilltoFlag(String billtoFlag) {
		this.billtoFlag = billtoFlag;
	}

	public String getCityName() {
		return this.cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}

	public String getCntryCd() {
		return this.cntryCd;
	}

	public void setCntryCd(String cntryCd) {
		this.cntryCd = cntryCd;
	}

	public String getCntryName() {
		return this.cntryName;
	}

	public void setCntryName(String cntryName) {
		this.cntryName = cntryName;
	}

	public String getCountyName() {
		return this.countyName;
	}

	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}

	public String getCrncyCd() {
		return this.crncyCd;
	}

	public void setCrncyCd(String crncyCd) {
		this.crncyCd = crncyCd;
	}

	public String getCustLvl2Name() {
		return this.custLvl2Name;
	}

	public void setCustLvl2Name(String custLvl2Name) {
		this.custLvl2Name = custLvl2Name;
	}

	public String getCustLvl2Nbr() {
		return this.custLvl2Nbr;
	}

	public void setCustLvl2Nbr(String custLvl2Nbr) {
		this.custLvl2Nbr = custLvl2Nbr;
	}

	public String getCustLvl3Name() {
		return this.custLvl3Name;
	}

	public void setCustLvl3Name(String custLvl3Name) {
		this.custLvl3Name = custLvl3Name;
	}

	public String getCustLvl3Nbr() {
		return this.custLvl3Nbr;
	}

	public void setCustLvl3Nbr(String custLvl3Nbr) {
		this.custLvl3Nbr = custLvl3Nbr;
	}

	public String getCustLvl4Name() {
		return this.custLvl4Name;
	}

	public void setCustLvl4Name(String custLvl4Name) {
		this.custLvl4Name = custLvl4Name;
	}

	public String getCustLvl4Nbr() {
		return this.custLvl4Nbr;
	}

	public void setCustLvl4Nbr(String custLvl4Nbr) {
		this.custLvl4Nbr = custLvl4Nbr;
	}

	public String getCustLvl5Name() {
		return this.custLvl5Name;
	}

	public void setCustLvl5Name(String custLvl5Name) {
		this.custLvl5Name = custLvl5Name;
	}

	public String getCustLvl5Nbr() {
		return this.custLvl5Nbr;
	}

	public void setCustLvl5Nbr(String custLvl5Nbr) {
		this.custLvl5Nbr = custLvl5Nbr;
	}

	public String getCustLvl6Name() {
		return this.custLvl6Name;
	}

	public void setCustLvl6Name(String custLvl6Name) {
		this.custLvl6Name = custLvl6Name;
	}

	public String getCustLvl6Nbr() {
		return this.custLvl6Nbr;
	}

	public void setCustLvl6Nbr(String custLvl6Nbr) {
		this.custLvl6Nbr = custLvl6Nbr;
	}

	public String getCustLvl7Name() {
		return this.custLvl7Name;
	}

	public void setCustLvl7Name(String custLvl7Name) {
		this.custLvl7Name = custLvl7Name;
	}

	public String getCustLvl7Nbr() {
		return this.custLvl7Nbr;
	}

	public void setCustLvl7Nbr(String custLvl7Nbr) {
		this.custLvl7Nbr = custLvl7Nbr;
	}

	public String getCustLvl8Name() {
		return this.custLvl8Name;
	}

	public void setCustLvl8Name(String custLvl8Name) {
		this.custLvl8Name = custLvl8Name;
	}

	public String getCustLvl8Nbr() {
		return this.custLvl8Nbr;
	}

	public void setCustLvl8Nbr(String custLvl8Nbr) {
		this.custLvl8Nbr = custLvl8Nbr;
	}

	public String getCustName() {
		return this.custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getCustNbr() {
		return this.custNbr;
	}

	public void setCustNbr(String custNbr) {
		this.custNbr = custNbr;
	}

	public String getCustSrchText() {
		return this.custSrchText;
	}

	public void setCustSrchText(String custSrchText) {
		this.custSrchText = custSrchText;
	}

	public String getCustStatCd() {
		return this.custStatCd;
	}

	public void setCustStatCd(String custStatCd) {
		this.custStatCd = custStatCd;
	}

	public String getCustStatDesc() {
		return this.custStatDesc;
	}

	public void setCustStatDesc(String custStatDesc) {
		this.custStatDesc = custStatDesc;
	}

	public String getCustTypCd() {
		return this.custTypCd;
	}

	public void setCustTypCd(String custTypCd) {
		this.custTypCd = custTypCd;
	}

	public String getCustTypDesc() {
		return this.custTypDesc;
	}

	public void setCustTypDesc(String custTypDesc) {
		this.custTypDesc = custTypDesc;
	}

	public String getDfltProdSrcFacCd() {
		return this.dfltProdSrcFacCd;
	}

	public void setDfltProdSrcFacCd(String dfltProdSrcFacCd) {
		this.dfltProdSrcFacCd = dfltProdSrcFacCd;
	}

	public String getDfltProdSrcFacDesc() {
		return this.dfltProdSrcFacDesc;
	}

	public void setDfltProdSrcFacDesc(String dfltProdSrcFacDesc) {
		this.dfltProdSrcFacDesc = dfltProdSrcFacDesc;
	}

	public String getDunsNbr() {
		return this.dunsNbr;
	}

	public void setDunsNbr(String dunsNbr) {
		this.dunsNbr = dunsNbr;
	}

	public String getEndUserFlag() {
		return this.endUserFlag;
	}

	public void setEndUserFlag(String endUserFlag) {
		this.endUserFlag = endUserFlag;
	}

	public int getEtlBatchId() {
		return this.etlBatchId;
	}

	public void setEtlBatchId(int etlBatchId) {
		this.etlBatchId = etlBatchId;
	}

	public Date getEtlCreateDtm() {
		return this.etlCreateDtm;
	}

	public void setEtlCreateDtm(Date etlCreateDtm) {
		this.etlCreateDtm = etlCreateDtm;
	}

	public String getEtlJobInvk() {
		return this.etlJobInvk;
	}

	public void setEtlJobInvk(String etlJobInvk) {
		this.etlJobInvk = etlJobInvk;
	}

	public String getEtlJobName() {
		return this.etlJobName;
	}

	public void setEtlJobName(String etlJobName) {
		this.etlJobName = etlJobName;
	}

	public Date getEtlLoadDtm() {
		return this.etlLoadDtm;
	}

	public void setEtlLoadDtm(Date etlLoadDtm) {
		this.etlLoadDtm = etlLoadDtm;
	}

	public String getEtlMsngFkFlag() {
		return this.etlMsngFkFlag;
	}

	public void setEtlMsngFkFlag(String etlMsngFkFlag) {
		this.etlMsngFkFlag = etlMsngFkFlag;
	}

	public String getFaxNbr() {
		return this.faxNbr;
	}

	public void setFaxNbr(String faxNbr) {
		this.faxNbr = faxNbr;
	}

	public String getHierExceptionCd() {
		return this.hierExceptionCd;
	}

	public void setHierExceptionCd(String hierExceptionCd) {
		this.hierExceptionCd = hierExceptionCd;
	}

	public String getHierExceptionFlag() {
		return this.hierExceptionFlag;
	}

	public void setHierExceptionFlag(String hierExceptionFlag) {
		this.hierExceptionFlag = hierExceptionFlag;
	}

	public String getHierTypCd() {
		return this.hierTypCd;
	}

	public void setHierTypCd(String hierTypCd) {
		this.hierTypCd = hierTypCd;
	}

	public String getHrmzCustName() {
		return this.hrmzCustName;
	}

	public void setHrmzCustName(String hrmzCustName) {
		this.hrmzCustName = hrmzCustName;
	}

	public String getHrmzCustNbr() {
		return this.hrmzCustNbr;
	}

	public void setHrmzCustNbr(String hrmzCustNbr) {
		this.hrmzCustNbr = hrmzCustNbr;
	}

	public String getIncoTerms1Cd() {
		return this.incoTerms1Cd;
	}

	public void setIncoTerms1Cd(String incoTerms1Cd) {
		this.incoTerms1Cd = incoTerms1Cd;
	}

	public String getIncoTerms1Desc() {
		return this.incoTerms1Desc;
	}

	public void setIncoTerms1Desc(String incoTerms1Desc) {
		this.incoTerms1Desc = incoTerms1Desc;
	}

	public Date getNewAcctDte() {
		return this.newAcctDte;
	}

	public void setNewAcctDte(Date newAcctDte) {
		this.newAcctDte = newAcctDte;
	}

	public String getPayerFlag() {
		return this.payerFlag;
	}

	public void setPayerFlag(String payerFlag) {
		this.payerFlag = payerFlag;
	}

	public String getPhnNbr() {
		return this.phnNbr;
	}

	public void setPhnNbr(String phnNbr) {
		this.phnNbr = phnNbr;
	}

	public String getPrimSlsrepCd() {
		return this.primSlsrepCd;
	}

	public void setPrimSlsrepCd(String primSlsrepCd) {
		this.primSlsrepCd = primSlsrepCd;
	}

	public String getPrimSlsrepName() {
		return this.primSlsrepName;
	}

	public void setPrimSlsrepName(String primSlsrepName) {
		this.primSlsrepName = primSlsrepName;
	}

	public String getPrimSlsrepUserid() {
		return this.primSlsrepUserid;
	}

	public void setPrimSlsrepUserid(String primSlsrepUserid) {
		this.primSlsrepUserid = primSlsrepUserid;
	}

	public BigDecimal getPrtclCustExtnlNteAmt() {
		return this.prtclCustExtnlNteAmt;
	}

	public void setPrtclCustExtnlNteAmt(BigDecimal prtclCustExtnlNteAmt) {
		this.prtclCustExtnlNteAmt = prtclCustExtnlNteAmt;
	}

	public BigDecimal getPrtclCustItrlNteAmt() {
		return this.prtclCustItrlNteAmt;
	}

	public void setPrtclCustItrlNteAmt(BigDecimal prtclCustItrlNteAmt) {
		this.prtclCustItrlNteAmt = prtclCustItrlNteAmt;
	}

	public String getPrtclCustName() {
		return this.prtclCustName;
	}

	public void setPrtclCustName(String prtclCustName) {
		this.prtclCustName = prtclCustName;
	}

	public String getPrtclCustNbr() {
		return this.prtclCustNbr;
	}

	public void setPrtclCustNbr(String prtclCustNbr) {
		this.prtclCustNbr = prtclCustNbr;
	}

	public BigDecimal getPrtclCustZoneChrgAmt() {
		return this.prtclCustZoneChrgAmt;
	}

	public void setPrtclCustZoneChrgAmt(BigDecimal prtclCustZoneChrgAmt) {
		this.prtclCustZoneChrgAmt = prtclCustZoneChrgAmt;
	}

	public String getPstlCd() {
		return this.pstlCd;
	}

	public void setPstlCd(String pstlCd) {
		this.pstlCd = pstlCd;
	}

	public String getPymtTermsCd() {
		return this.pymtTermsCd;
	}

	public void setPymtTermsCd(String pymtTermsCd) {
		this.pymtTermsCd = pymtTermsCd;
	}

	public String getPymtTermsDesc() {
		return this.pymtTermsDesc;
	}

	public void setPymtTermsDesc(String pymtTermsDesc) {
		this.pymtTermsDesc = pymtTermsDesc;
	}

	public String getSapAcctAsgnGrpCd() {
		return this.sapAcctAsgnGrpCd;
	}

	public void setSapAcctAsgnGrpCd(String sapAcctAsgnGrpCd) {
		this.sapAcctAsgnGrpCd = sapAcctAsgnGrpCd;
	}

	public String getSapAcctAsgnGrpDesc() {
		return this.sapAcctAsgnGrpDesc;
	}

	public void setSapAcctAsgnGrpDesc(String sapAcctAsgnGrpDesc) {
		this.sapAcctAsgnGrpDesc = sapAcctAsgnGrpDesc;
	}

	public String getSapCustAcctGrpCd() {
		return this.sapCustAcctGrpCd;
	}

	public void setSapCustAcctGrpCd(String sapCustAcctGrpCd) {
		this.sapCustAcctGrpCd = sapCustAcctGrpCd;
	}

	public String getSapCustAcctGrpDesc() {
		return this.sapCustAcctGrpDesc;
	}

	public void setSapCustAcctGrpDesc(String sapCustAcctGrpDesc) {
		this.sapCustAcctGrpDesc = sapCustAcctGrpDesc;
	}

	public String getSapCustClsCd() {
		return this.sapCustClsCd;
	}

	public void setSapCustClsCd(String sapCustClsCd) {
		this.sapCustClsCd = sapCustClsCd;
	}

	public String getSapCustClsDesc() {
		return this.sapCustClsDesc;
	}

	public void setSapCustClsDesc(String sapCustClsDesc) {
		this.sapCustClsDesc = sapCustClsDesc;
	}

	public String getSapCustGrpCd() {
		return this.sapCustGrpCd;
	}

	public void setSapCustGrpCd(String sapCustGrpCd) {
		this.sapCustGrpCd = sapCustGrpCd;
	}

	public String getSapCustGrpDesc() {
		return this.sapCustGrpDesc;
	}

	public void setSapCustGrpDesc(String sapCustGrpDesc) {
		this.sapCustGrpDesc = sapCustGrpDesc;
	}

	public String getSapCustGrp3Cd() {
		return this.sapCustGrp3Cd;
	}

	public void setSapCustGrp3Cd(String sapCustGrp3Cd) {
		this.sapCustGrp3Cd = sapCustGrp3Cd;
	}

	public String getSapCustGrp3Desc() {
		return this.sapCustGrp3Desc;
	}

	public void setSapCustGrp3Desc(String sapCustGrp3Desc) {
		this.sapCustGrp3Desc = sapCustGrp3Desc;
	}

	public String getSapDistChnlCd() {
		return this.sapDistChnlCd;
	}

	public void setSapDistChnlCd(String sapDistChnlCd) {
		this.sapDistChnlCd = sapDistChnlCd;
	}

	public String getSapDistChnlName() {
		return this.sapDistChnlName;
	}

	public void setSapDistChnlName(String sapDistChnlName) {
		this.sapDistChnlName = sapDistChnlName;
	}

	public String getSapDivCd() {
		return this.sapDivCd;
	}

	public void setSapDivCd(String sapDivCd) {
		this.sapDivCd = sapDivCd;
	}

	public String getSapDivName() {
		return this.sapDivName;
	}

	public void setSapDivName(String sapDivName) {
		this.sapDivName = sapDivName;
	}

	public String getSapSlsGrpCd() {
		return this.sapSlsGrpCd;
	}

	public void setSapSlsGrpCd(String sapSlsGrpCd) {
		this.sapSlsGrpCd = sapSlsGrpCd;
	}

	public String getSapSlsGrpDesc() {
		return this.sapSlsGrpDesc;
	}

	public void setSapSlsGrpDesc(String sapSlsGrpDesc) {
		this.sapSlsGrpDesc = sapSlsGrpDesc;
	}

	public String getSapSlsOfficeCd() {
		return this.sapSlsOfficeCd;
	}

	public void setSapSlsOfficeCd(String sapSlsOfficeCd) {
		this.sapSlsOfficeCd = sapSlsOfficeCd;
	}

	public String getSapSlsOfficeDesc() {
		return this.sapSlsOfficeDesc;
	}

	public void setSapSlsOfficeDesc(String sapSlsOfficeDesc) {
		this.sapSlsOfficeDesc = sapSlsOfficeDesc;
	}

	public String getSapSlsOrgCd() {
		return this.sapSlsOrgCd;
	}

	public void setSapSlsOrgCd(String sapSlsOrgCd) {
		this.sapSlsOrgCd = sapSlsOrgCd;
	}

	public String getSapSlsOrgName() {
		return this.sapSlsOrgName;
	}

	public void setSapSlsOrgName(String sapSlsOrgName) {
		this.sapSlsOrgName = sapSlsOrgName;
	}

	public String getSatCustTypChnlCd() {
		return this.satCustTypChnlCd;
	}

	public void setSatCustTypChnlCd(String satCustTypChnlCd) {
		this.satCustTypChnlCd = satCustTypChnlCd;
	}

	public String getSatCustTypChnlDesc() {
		return this.satCustTypChnlDesc;
	}

	public void setSatCustTypChnlDesc(String satCustTypChnlDesc) {
		this.satCustTypChnlDesc = satCustTypChnlDesc;
	}

	public String getScndrySlsrepCd() {
		return this.scndrySlsrepCd;
	}

	public void setScndrySlsrepCd(String scndrySlsrepCd) {
		this.scndrySlsrepCd = scndrySlsrepCd;
	}

	public String getScndrySlsrepName() {
		return this.scndrySlsrepName;
	}

	public void setScndrySlsrepName(String scndrySlsrepName) {
		this.scndrySlsrepName = scndrySlsrepName;
	}

	public String getScndrySlsrepUserid() {
		return this.scndrySlsrepUserid;
	}

	public void setScndrySlsrepUserid(String scndrySlsrepUserid) {
		this.scndrySlsrepUserid = scndrySlsrepUserid;
	}

	public String getSdwCustSsk() {
		return this.sdwCustSsk;
	}

	public void setSdwCustSsk(String sdwCustSsk) {
		this.sdwCustSsk = sdwCustSsk;
	}

	public String getShiptoFlag() {
		return this.shiptoFlag;
	}

	public void setShiptoFlag(String shiptoFlag) {
		this.shiptoFlag = shiptoFlag;
	}

	public String getSoldtoFlag() {
		return this.soldtoFlag;
	}

	public void setSoldtoFlag(String soldtoFlag) {
		this.soldtoFlag = soldtoFlag;
	}

	public String getSrcSysCd() {
		return this.srcSysCd;
	}

	public void setSrcSysCd(String srcSysCd) {
		this.srcSysCd = srcSysCd;
	}

	public String getSrcSysDelFlag() {
		return this.srcSysDelFlag;
	}

	public void setSrcSysDelFlag(String srcSysDelFlag) {
		this.srcSysDelFlag = srcSysDelFlag;
	}

	public String getStateCd() {
		return this.stateCd;
	}

	public void setStateCd(String stateCd) {
		this.stateCd = stateCd;
	}

	public String getStateName() {
		return this.stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getTaxIdNbr() {
		return this.taxIdNbr;
	}

	public void setTaxIdNbr(String taxIdNbr) {
		this.taxIdNbr = taxIdNbr;
	}

	public int getTermsNetDys() {
		return this.termsNetDys;
	}

	public void setTermsNetDys(int termsNetDys) {
		this.termsNetDys = termsNetDys;
	}

	public int getUltmPrntCustId() {
		return this.ultmPrntCustId;
	}

	public void setUltmPrntCustId(int ultmPrntCustId) {
		this.ultmPrntCustId = ultmPrntCustId;
	}

	public String getUltmPrntCustName() {
		return this.ultmPrntCustName;
	}

	public void setUltmPrntCustName(String ultmPrntCustName) {
		this.ultmPrntCustName = ultmPrntCustName;
	}

	public String getUltmPrntCustNbr() {
		return this.ultmPrntCustNbr;
	}

	public void setUltmPrntCustNbr(String ultmPrntCustNbr) {
		this.ultmPrntCustNbr = ultmPrntCustNbr;
	}
}
