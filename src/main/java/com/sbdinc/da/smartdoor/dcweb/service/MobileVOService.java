package com.sbdinc.da.smartdoor.dcweb.service;

import com.sbdinc.da.smartdoor.dcweb.model.DoorControllerFact;
import com.sbdinc.da.smartdoor.dcweb.model.MobileVO;
import com.sbdinc.da.smartdoor.dcweb.model.TechVisit;

/**
 *
 *
 * @author Dian Jiao
 *
 */
public interface MobileVOService {
	TechVisit voToTechVisit(MobileVO vo);
	DoorControllerFact voToDoorControllerFact(MobileVO vo);

}
