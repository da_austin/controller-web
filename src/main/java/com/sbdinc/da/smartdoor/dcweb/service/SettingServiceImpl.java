package com.sbdinc.da.smartdoor.dcweb.service;

import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.sbdinc.da.smartdoor.dcweb.model.Setting;
import com.sbdinc.da.smartdoor.dcweb.repository.SettingRepository;
import com.sbdinc.da.smartdoor.dcweb.util.XMLParser;

/**
 * Implementation class for Setting Service
 * 
 * @author Dian Jiao
 *
 */
@Service
public class SettingServiceImpl implements SettingService {
	private static final Logger logger = LoggerFactory.getLogger(SettingServiceImpl.class);

	final private SimpleDateFormat FORMATTER = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
	
	@Autowired
	DoorControllerFactService doorControllerFactService;
	
	@Autowired
	SettingRepository settingRepository;
	
	@Override
	public long saveSetting(String xml, long msgId, long visitId) throws ParserConfigurationException, SAXException, IOException {
		return parseSettingXML(xml, msgId, visitId);		
	}

	/**
	 * Parse settings xml
	 * @throws ParserConfigurationException 
	 * @throws IOException 
	 * @throws SAXException 
	 */
	private long parseSettingXML(String xml, long msgId, long visitId) throws SAXException, IOException, ParserConfigurationException {
		long settingId = 0;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    InputSource is = new InputSource(new StringReader(xml));
	    Document doc = builder.parse(is);
	    doc.getDocumentElement().normalize();
	    Element root = doc.getDocumentElement();
	    NodeList children = root.getChildNodes();
	    
	    // parse header
	    String doorId = XMLParser.getNodeValue("activeDoorID", children);
	    long controllerId = doorControllerFactService.findIDbyDoorId(doorId);
	    
	    // Check whether the matching controller exists
	    if (controllerId == 0) {
    		logger.error("No matching controller exists in ControllerDoor table with doorId (" + doorId + ")");
	    	return 0;		    		  	
	    }
	    String serialNumber = XMLParser.getNodeValue("controllerSerialNumber", children);
	    String date = XMLParser.getNodeValue("date", children);
	    String opCode = XMLParser.getNodeValue("op_mode", children);
	    String fwLogicVers = XMLParser.getNodeValue("fw_rev", children);
	    String fwMotionVers = XMLParser.getNodeValue("fwMotionVersion", children);
	    String encType1 = XMLParser.getNodeValue("enc_type1", children);
	    String encType2 = XMLParser.getNodeValue("enc_type2", children);
	    String handing1 = XMLParser.getNodeValue("handing1", children);
	    String handing2 = XMLParser.getNodeValue("handing2", children);
	    
	    // parse param nodes
	    List<Node> params = XMLParser.getNodes("param", children);
	    Date insertTs = new Date();
	    Setting setting = new Setting();
	    setting.setDoorId(doorId);
	    setting.setControllerSerialNumber(serialNumber);
	    setting.setMessageId(msgId);
	    setting.setVisitId(visitId);
	    if (!StringUtils.isEmpty(date))
	    	try {
	    		setting.setFirstReadDt(FORMATTER.parse(date));
	    	} catch (ParseException e){
	    		logger.warn("ParseException FirstReadDt");
	    		
	    	}
	    setting.setOpCode(opCode);
	    setting.setFwLogicVer(fwLogicVers);
	    setting.setFwMotionVer(fwMotionVers);
	    setting.setInsertTS(insertTs);
	    setting.setM1Encoder(encType1);
		setting.setM2Encoder(encType2);
	    setting.setM1Handing(handing1);
		setting.setM2Handing(handing2);
	    
	    for (Node param : params) {	    		    
	    	NodeList seeds = param.getChildNodes();
	    	String name = XMLParser.getNodeValue("name", seeds);
	    	if (name == "") continue;
	    	
	    	if (name.equalsIgnoreCase("open speed")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1OpenSpeed(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2OpenSpeed(Integer.parseInt(value2));
	    	}	
	    	if (name.equalsIgnoreCase("open torque")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1OpenTorque(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2OpenTorque(Integer.parseInt(value2));
	    	}	
	    	if (name.equalsIgnoreCase("open acceleration")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1OpenAcceleration(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2OpenAcceleration(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("check speed")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1CheckSpeed(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2CheckSpeed(Integer.parseInt(value2));
	    	}	
	    	if (name.equalsIgnoreCase("check torque")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1CheckTorque(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2CheckTorque(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("door obstruction time")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1DoorObstructionTime(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2DoorObstructionTime(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("open obstruction time")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1OpenObstructionTime(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2OpenObstructionTime(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("open braking")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1OpenBraking(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2OpenBraking(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("open check speed")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1OpenCheckSpeed(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2OpenCheckSpeed(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("open check torque")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1OpenCheckTorque(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2OpenCheckTorque(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("open check percent")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1OpenCheckPercent(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2OpenCheckPercent(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("open check obstruction time")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1OpenCheckObstructionTime(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2OpenCheckObstructionTime(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("open check length")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1OpenCheckLen(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2OpenCheckLen(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("open check boost")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1OpenCheckBoost(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2OpenCheckBoost(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("open learn speed")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1OpenLearnSpeed(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2OpenLearnSpeed(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("open learn torque")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1OpenLearnTorque(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2OpenLearnTorque(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("open startup torque")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1OpenStartupTorque(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2OpenStartupTorque(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("open startup obstruction time")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1OpenStartupObsTime(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2OpenStartupObsTime(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("open startup length")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1OpenStartupLen(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2OpenStartupLen(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("hold open delay")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1HoldOpenDelay(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2HoldOpenDelay(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("close check obstruction time")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1CloseCheckObsTime(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2CloseCheckObsTime(Integer.parseInt(value2));
	    	}
	    	
	    	//Shared attrbitues for Swing and Slide but only one motor
	    	if (name.equalsIgnoreCase("safety logic")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setSafetyLogic(Integer.parseInt(value1));
	    	}
	    	if (name.equalsIgnoreCase("hold beam type")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setHoldBeamType(Integer.parseInt(value1));
	    	}
	    	if (name.equalsIgnoreCase("lock logic")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setLockLogic(Integer.parseInt(value1));
	    	}
	    	if (name.equalsIgnoreCase("2s operation")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setTwos(Integer.parseInt(value1));
	    	}
	    	if (name.equalsIgnoreCase("lock release torque")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setLockReleaseTorque(Integer.parseInt(value1));
	    	}
	    	// "lock press time" and "lock release time" map to same col lock_release_tm
	    	if (name.equalsIgnoreCase("lock press time")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setLockReleaseTime(Integer.parseInt(value1));
	    	}
	    	if (name.equalsIgnoreCase("lock release time")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setLockReleaseTime(Integer.parseInt(value1));
	    	}
	    	
	    	if (name.equalsIgnoreCase("lock delay")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setLockDelay(Integer.parseInt(value1));
	    	}
	    	
	    	if (name.equalsIgnoreCase("recycle speed")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1)) 
	    			setting.setM1RecycleSpeed(Integer.parseInt(value1));
	    		if (isInteger(value2)) 
	    			setting.setM2RecycleSpeed(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("open stop")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1)) 
	    			setting.setM1OpenStop(Integer.parseInt(value1));
	    		if (isInteger(value2)) 
	    			setting.setM2OpenStop(Integer.parseInt(value2));
	    	}
	    	
	    	// "reduced open length" and "reduced open percent" map to the same column
	    	if (name.equalsIgnoreCase("reduced open length")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setReducedOpenLen(Integer.parseInt(value1));
	    	}
	    	if (name.equalsIgnoreCase("reduced open percent")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setReducedOpenLen(Integer.parseInt(value1));
	    	}
	    	
	    	if (name.equalsIgnoreCase("close learn speed")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setCloseLearnSpeed(Integer.parseInt(value1));
	    	}
	    	if (name.equalsIgnoreCase("close learn torque")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setCloseLearnTorque(Integer.parseInt(value1));
	    	}
	    	if (name.equalsIgnoreCase("close startup torque")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setCloseStartupTorque(Integer.parseInt(value1));
	    	}
	    	if (name.equalsIgnoreCase("close startup length")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setCloseStartupLen(Integer.parseInt(value1));
	    	}
	    	if (name.equalsIgnoreCase("close speed")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setCloseSpeed(Integer.parseInt(value1));
	    	}
	    	if (name.equalsIgnoreCase("close torque")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setCloseTorque(Integer.parseInt(value1));
	    	}
	    	if (name.equalsIgnoreCase("close acceleration")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setCloseAccl(Integer.parseInt(value1));
	    	}
	    	if (name.equalsIgnoreCase("close obstruction time")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1)) 
	    			setting.setM1CloseObsTime(Integer.parseInt(value1));
	    		if (isInteger(value2)) 
	    			setting.setM2CloseObsTime(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("Magic Touch Feel")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1)) 
	    			setting.setM1MagicTouchFeel(Integer.parseInt(value1));
	    		if (isInteger(value2)) 
	    			setting.setM2MagicTouchFeel(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("Stall Sensor Input")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1)) 
	    			setting.setM1StallSensorInput(Integer.parseInt(value1));
	    		if (isInteger(value2)) 
	    			setting.setM2StallSensorInput(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("Motor Starting Speed")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1)) 
	    			setting.setM1MotorStartSpeed(Integer.parseInt(value1));
	    		if (isInteger(value2)) 
	    			setting.setM2MotorStartSpeed(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("Creep Length")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1)) 
	    			setting.setM1CreepLength(Integer.parseInt(value1));
	    		if (isInteger(value2)) 
	    			setting.setM2CreepLength(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("Preload Torque")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1)) 
	    			setting.setM1PreloadTorque(Integer.parseInt(value1));
	    		if (isInteger(value2)) 
	    			setting.setM2PreloadTorque(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("Preload Mode")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1)) 
	    			setting.setM1PreloadMode(Integer.parseInt(value1));
	    		if (isInteger(value2)) 
	    			setting.setM2PreloadMode(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("Power Assist Sensitivity")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1)) 
	    			setting.setM1PowerAssistSensitivity(Integer.parseInt(value1));
	    		if (isInteger(value2)) 
	    			setting.setM2PowerAssistSensitivity(Integer.parseInt(value2));
	    	}
	    	
	    	if (name.equalsIgnoreCase("close check percent")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setCloseCheckPct(Integer.parseInt(value1));
	    	}
	    	if (name.equalsIgnoreCase("close check speed")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setCloseCheckSpeed(Integer.parseInt(value1));
	    	}
	    	if (name.equalsIgnoreCase("close check torque")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setCloseCheckTorque(Integer.parseInt(value1));
	    	}
	    	if (name.equalsIgnoreCase("close braking")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setCloseBraking(Integer.parseInt(value1));
	    	}
	    	// "close press ramp" and "close press" map to the same col close_press
	    	if (name.equalsIgnoreCase("close press ramp")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setClosePress(Integer.parseInt(value1));
	    	}
	    	if (name.equalsIgnoreCase("close press")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setClosePress(Integer.parseInt(value1));
	    	}
	    	if (name.equalsIgnoreCase("function switch type")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setFuncSwitchType(Integer.parseInt(value1));
	    	}
	    	if (name.equalsIgnoreCase("lock time")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setLockTime(Integer.parseInt(value1));
	    	}
	    	if (name.equalsIgnoreCase("delayed egress")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setDelayedEgress(Integer.parseInt(value1));
	    	}
	    	if (name.equalsIgnoreCase("access control pro")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setAccessCtrlPro(Integer.parseInt(value1));
	    	}
	    	if (name.equalsIgnoreCase("io configuration")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setIoConfig(Integer.parseInt(value1));
	    	}
	    	if (name.equalsIgnoreCase("fire alarm mode")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setFireAlarmMode(Integer.parseInt(value1));
	    	}
	    	if (name.equalsIgnoreCase("close learn ")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1)) 
	    			setting.setFireAlarmMode(Integer.parseInt(value1));
	    	}
	    	
	    	if (name.equalsIgnoreCase("full open offset")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1FullOpenOffset(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2FullOpenOffset(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("full open torque")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1FullOpenTorque(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2FullOpenTorque(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("magic touch sensitivity")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1MtSensitivity(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2MtSensitivity(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("magic touch hold open time")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1MtHoldOpenTime(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2MtHoldOpenTime(Integer.parseInt(value2));
	    	}
	    	
	    	// "Close Check Length" and "Close Position Tolerance" map to same col
	    	if (name.equalsIgnoreCase("close check length")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1CloseCheckLen(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2CloseCheckLen(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("close position tolerance")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1CloseCheckLen(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2CloseCheckLen(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("close check boost")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1CloseCheckBoost(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2CloseCheckBoost(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("power close speed")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1PowerCloseSpeed(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2PowerCloseSpeed(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("power close torque")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1PowerCloseTorque(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2PowerCloseTorque(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("power close obstruction time")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1PowerCloseObsTime(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2PowerCloseObsTime(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("power close length")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1PowerCloseLen(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2PowerCloseLen(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("power close time")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1PowerCloseTm(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2PowerCloseTm(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("stall speed")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1StallSpeed(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2StallSpeed(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("stall torque")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1StallTorque(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2StallTorque(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("operate delay")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1OpDelay(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2OpDelay(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("door travel")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1DoorTravel(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2DoorTravel(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("overspeed compensation rate")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1))
	    			setting.setOverspeedCompRate(Integer.parseInt(value1));
	    	}
	    	if (name.equalsIgnoreCase("unlock time")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1))
	    			setting.setUnlockTime(Integer.parseInt(value1));
	    	}
	    	if (name.equalsIgnoreCase("manual mode sensor override")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1))
	    			setting.setSensorOverride(Integer.parseInt(value1));
	    	}
	    	if (name.equalsIgnoreCase("off mode")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1))
	    			setting.setOffMode(Integer.parseInt(value1));
	    	}  
	    	if (name.equalsIgnoreCase("no reverse on obstruction")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1))
	    			setting.setNoRevObs(Integer.parseInt(value1));
	    	} 
	    	if (name.equalsIgnoreCase("option 3")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1))
	    			setting.setOptionThree(Integer.parseInt(value1));
	    	} 
	    	if (name.equalsIgnoreCase("option 4")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1))
	    			setting.setOptionFour(Integer.parseInt(value1));
	    	} 
	    	if (name.equalsIgnoreCase("open power assist torque")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1))
	    			setting.setOpenPowerAssistTorque(Integer.parseInt(value1));
	    	} 
	    	if (name.equalsIgnoreCase("close power assist torque")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		if (isInteger(value1))
	    			setting.setClosePowerAssistTorque(Integer.parseInt(value1));
	    	}
	    	if (name.equalsIgnoreCase("open learn obstruction time")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1OpenLearnObstructionTime(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2OpenLearnObstructionTime(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("open startup obstruction time")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1OpenStartupObstructionTime(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2OpenStartupObstructionTime(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("power assist torque")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1PowerAssistTorque(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2PowerAssistTorque(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("power assist exit angle")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1PowerAssistExitAngle(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2PowerAssistExitAngle(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("power assist trigger threshold")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1PowerAssistTriggerThreshold(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2PowerAssistTriggerThreshold(Integer.parseInt(value2));
	    	}
	    	if (name.equalsIgnoreCase("creep speed")) {
	    		String value1 = XMLParser.getNodeValue("valueForMotor1", seeds);
	    		String value2 = XMLParser.getNodeValue("valueForMotor2", seeds);
	    		if (isInteger(value1))
	    			setting.setM1CreepSpeed(Integer.parseInt(value1));
	    		if (isInteger(value2))
	    			setting.setM2CreepSpeed(Integer.parseInt(value2));
	    	}
	    }
	    
	    //persist
	    settingRepository.save(setting);
	    settingId = setting.getId();
		    
		return settingId;
	}
		
	private boolean isInteger(String s) {
	    try
	    {
	        Integer.parseInt(s);
	        return true;
	    } catch (NumberFormatException ex)
	    {
	        return false;
	    }
	}

}
