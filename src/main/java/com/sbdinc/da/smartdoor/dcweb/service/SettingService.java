package com.sbdinc.da.smartdoor.dcweb.service;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/**
 * Service interface for Settings
 * 
 * @author Dian Jiao
 *
 */
public interface SettingService {
	long saveSetting(String xml, long msgId, long visitId) throws ParserConfigurationException, SAXException, IOException;

}
