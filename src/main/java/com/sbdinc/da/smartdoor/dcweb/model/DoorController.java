package com.sbdinc.da.smartdoor.dcweb.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "DOOR_CTRLR_DIM")
public class DoorController implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DOOR_CTRLR_ID")
    private long id;
	
	@Column(name = "ACTIVE_CTRLR_SERL_NBR")
	private String activeControllerSerialNumber;
	
	@Column(name = "ACTIVE_DOOR_ID")
	private String activeDoorId;
	
	@Column(name = "CTRLR_TYP")
	private String controllerType;
	
	@Column(name = "LCP_VERS")
	private String lcpVersion;
	
	@Column(name = "MCP_VERS")
	private String mcpVersion;
	
	@Column(name = "MSG_ID")
	private long messageID;
	
	@Column(name = "ORIG_CTRLR_SERL_NBR")
	private String originalControllerSerialNumber;
	
	@Column(name = "ORIG_DOOR_ID")
	private String originalDoorId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ETL_INSERT_TS", insertable=true, updatable=false)
	private Date insertTs;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ETL_UPDATE_TS", insertable=false, updatable=true)
	private Date updateTs;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getActiveControllerSerialNumber() {
		return activeControllerSerialNumber;
	}

	public void setActiveControllerSerialNumber(String activeControllerSerialNumber) {
		this.activeControllerSerialNumber = activeControllerSerialNumber;
	}

	public String getActiveDoorId() {
		return activeDoorId;
	}

	public void setActiveDoorId(String activeDoorId) {
		this.activeDoorId = activeDoorId;
	}

	public String getControllerType() {
		return controllerType;
	}

	public void setControllerType(String controllerType) {
		this.controllerType = controllerType;
	}

	public String getLcpVersion() {
		return lcpVersion;
	}

	public void setLcpVersion(String lcpVersion) {
		this.lcpVersion = lcpVersion;
	}

	public String getMcpVersion() {
		return mcpVersion;
	}

	public void setMcpVersion(String mcpVersion) {
		this.mcpVersion = mcpVersion;
	}

	public long getMessageID() {
		return messageID;
	}

	public void setMessageID(long messageID) {
		this.messageID = messageID;
	}

	public String getOriginalControllerSerialNumber() {
		return originalControllerSerialNumber;
	}

	public void setOriginalControllerSerialNumber(String originalControllerSerialNumber) {
		this.originalControllerSerialNumber = originalControllerSerialNumber;
	}

	public String getOriginalDoorId() {
		return originalDoorId;
	}

	public void setOriginalDoorId(String originalDoorId) {
		this.originalDoorId = originalDoorId;
	}

	public Date getInsertTS() {
		return insertTs;
	}

	public void setInsertTS(Date insertTS) {
		this.insertTs = insertTS;
	}

	public Date getUpdateTS() {
		return updateTs;
	}

	public void setUpdateTS(Date updateTS) {
		this.updateTs = updateTS;
	}
	
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("activeControllerSerialNumber: ").append(activeControllerSerialNumber);
		sb.append("\nactiveDoorId: ").append(activeDoorId);
		sb.append("\nmessageID: ").append(messageID);
		sb.append("\ncontrollerType").append(controllerType);
		sb.append("\nlcpVersion").append(lcpVersion);
		sb.append("\nmcpVersion").append(mcpVersion);
		sb.append("originalControllerSerialNumber: ").append(originalControllerSerialNumber);
		sb.append("\noriginalDoorId: ").append(originalDoorId);
		return sb.toString();
	}

}
