package com.sbdinc.da.smartdoor.dcweb.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entity class for TechVisit
 * 
 * @author Dian Jiao
 *
 */

@Entity
@Table(name = "TCHN_VISIT_HIST")
public class TechVisit implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "TCHN_VISIT_ID")
    private long id;
	
	@Column(name = "DOOR_ID")
	private String activeDoorId;
	
	@Column(name = "MSG_ID")
	private long messageId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "TCHN_READING_DTM")
	private Date readingDateTime;
	
	@Column(name = "TCHN_READING_TIMEZONE")
	private int readingTimeZone;
	
	@Column(name = "READING_LATTITUDE")
	private BigDecimal lattitude;
	
	@Column(name = "READING_LONGITUDE")
	private BigDecimal longitude;

	@Column(name = "ANDROID_ID")
	private String androidId;
	
	@Column(name = "API_LEVEL")
	private int apiLevel;
	
	@Column(name = "PHN_NBR")
	private String phoneNumber;
	
	@Column(name = "EMAIL_ADDR")
	private String emailAddress;
	
	@Column(name = "MAC_ADDR")
	private String macAddress;
	
	@Column(name = "APP_VERS")
	private BigDecimal appVersion;
	
	@Column(name = "APP_DB_VERS")
	private String appDbVersion;
	
	@Column(name = "door_id_populate_method")
	private String doorIdPopulateMethod;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ETL_INSERT_TS", insertable=true, updatable=false)
	private Date insertTS;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ETL_UPDATE_TS", insertable=false, updatable=true)
	private Date updateTS;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getActiveDoorId() {
		return activeDoorId;
	}

	public void setActiveDoorId(String activeDoorId) {
		this.activeDoorId = activeDoorId;
	}

	public long getMessageId() {
		return messageId;
	}

	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}

	public Date getReadingDateTime() {
		return readingDateTime;
	}

	public void setReadingDateTime(Date readDt) {
		this.readingDateTime = readDt;
	}

	public int getReadingTimeZone() {
		return readingTimeZone;
	}

	public void setReadingTimeZone(int readTz) {
		this.readingTimeZone = readTz;
	}

	public BigDecimal getLattitude() {
		return lattitude;
	}

	public void setLattitude(BigDecimal lattitude) {
		this.lattitude = lattitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public String getAndroidId() {
		return androidId;
	}

	public void setAndroidId(String androidId) {
		this.androidId = androidId;
	}

	public int getApiLevel() {
		return apiLevel;
	}

	public void setApiLevel(int apiLevel) {
		this.apiLevel = apiLevel;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public BigDecimal getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(BigDecimal appVersion) {
		this.appVersion = appVersion;
	}

	public String getAppDbVersion() {
		return appDbVersion;
	}

	public void setAppDbVersion(String appDbVersion) {
		this.appDbVersion = appDbVersion;
	}

	public String getDoorIdPopulateMethod() {
		return doorIdPopulateMethod;
	}

	public void setDoorIdPopulateMethod(String doorIdPopulateMethod) {
		this.doorIdPopulateMethod = doorIdPopulateMethod;
	}

	public Date getInsertTS() {
		return insertTS;
	}

	public void setInsertTS(Date insertTS) {
		this.insertTS = insertTS;
	}

	public Date getUpdateTS() {
		return updateTS;
	}

	public void setUpdateTS(Date updateTS) {
		this.updateTS = updateTS;
	}
	
	public String toString() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		StringBuilder sb = new StringBuilder();
		sb.append("doorId: ").append(activeDoorId);
		sb.append("\nreadingDateTime: ");
		if (readingDateTime != null)
			sb.append(df.format(readingDateTime));
		sb.append("\nreadingTimeZone: ").append(Integer.toString(readingTimeZone));
		sb.append("\nlattitude: ").append(lattitude);
		sb.append("\nlongitude: ").append(longitude);
		sb.append("\nphoneNumber: ").append(phoneNumber);
		sb.append("\nmacAddress: ").append(macAddress);
		sb.append("\nappVersion: ").append(appVersion);
		sb.append("\nappDbVersion: ").append(appDbVersion);
		return sb.toString();
	}

}
