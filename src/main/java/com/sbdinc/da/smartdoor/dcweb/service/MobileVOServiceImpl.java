package com.sbdinc.da.smartdoor.dcweb.service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.sbdinc.da.smartdoor.dcweb.model.DoorControllerFact;
import com.sbdinc.da.smartdoor.dcweb.model.MobileVO;
import com.sbdinc.da.smartdoor.dcweb.model.TechVisit;
import com.sbdinc.da.smartdoor.dcweb.util.AESEncryptor;
import com.sbdinc.da.smartdoor.dcweb.util.DateTimeConverter;

/**
 * Implementation class for MobileVOService
 *
 * @author Dian Jiao
 *
 */
@Service
public class MobileVOServiceImpl implements MobileVOService {
	private static final Logger logger = LoggerFactory.getLogger(MobileVOServiceImpl.class);
	
	// Convert MobileVO object to TechVisit object
	@Override
	public TechVisit voToTechVisit(MobileVO vo) {
		TechVisit tv = new TechVisit();
		tv.setActiveDoorId(vo.getActiveDoorId());
		LocalDateTime dt = null;
		if (vo.getReadingDateTime() != null) 
			dt = DateTimeConverter.stringToDateTime(vo.getReadingDateTime());
		if (dt != null) {
			Instant instant = dt.toInstant(ZoneOffset.UTC);
		    Date date = Date.from(instant);
		    tv.setReadingDateTime(date);
		}
		tv.setReadingTimeZone(vo.getReadingTimeZone());
		tv.setLattitude(vo.getLattitude());
		tv.setLongitude(vo.getLongitude());
		tv.setAndroidId(vo.getAndroidId());
		tv.setApiLevel(vo.getApiLevel());
		tv.setMacAddress(vo.getMacAddress());
		tv.setPhoneNumber(vo.getPhoneNumber());
		try {
			String email = AESEncryptor.decrypt(vo.getEmailAddress());
			tv.setEmailAddress(email);
		} catch (Exception e) {
			logger.warn(e.getMessage());
		}
		tv.setAppVersion(vo.getAppVersion());
		tv.setAppDbVersion(vo.getAppDbVersion());
		tv.setDoorIdPopulateMethod(vo.getDoorIdPopulateMethod());
		return tv;
	}

	// Convert MobileVO object to CycleCount object
	@Override
	public DoorControllerFact voToDoorControllerFact(MobileVO vo) {
		DoorControllerFact dc = new DoorControllerFact();
		dc.setDoorId(vo.getActiveDoorId());
		dc.setSystemCycleCount(vo.getSystemCycleCount());
		dc.setControllerCycleCount(vo.getControllerCycleCount());
		dc.setHealthCd(vo.getDoorHealthCode());
		dc.setLcpVersion(vo.getLcpVersion());
		dc.setMcpVersion(vo.getMcpVersion());
		dc.setControllerSerialNumber(vo.getOriginalControllerSerialNumber());
		return dc;
	}

}
