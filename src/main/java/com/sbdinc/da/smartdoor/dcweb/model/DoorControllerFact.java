package com.sbdinc.da.smartdoor.dcweb.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * Entity class of CycleCount
 * 
 * @author Dian Jiao
 *
 */
@Entity
@Table(name = "DOOR_CTRLR_FACT")
public class DoorControllerFact implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DOOR_CTRLR_FACT_ID")
    private long id;
	
	@Column(name = "DOOR_ID")
	private String doorId;
	
	@Column(name = "VISIT_ID")
	private long visitId;
	
	@Column(name = "LCP_VERS")
	private String lcpVersion;
	
	@Column(name = "MCP_VERS")
	private String mcpVersion;
	
	@Column(name = "MSG_ID")
	private long messageId;
	
	@Column(name = "CTRLR_SERL_NBR")
	private String controllerSerialNumber;
	
	@Column(name = "SYS_CYCLE_CNT")
	private int systemCycleCount;
	
	@Column(name = "CTRLR_CYCLE_CNT")
	private int controllerCycleCount;
	
	@Column(name = "RSSI_SIG_STR")
	private int rssiSignalStrength;
	
	@Column(name = "DOOR_HEALTH_CD")
	private String healthCd;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ETL_INSERT_TS", insertable=true, updatable=false)
	private Date insertTs;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ETL_UPDATE_TS", insertable=false, updatable=true)
	private Date updateTs;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDoorId() {
		return doorId;
	}

	public void setDoorId(String doorId) {
		this.doorId = doorId;
	}

	public String getLcpVersion() {
		return lcpVersion;
	}

	public void setLcpVersion(String lcpVersion) {
		this.lcpVersion = lcpVersion;
	}

	public String getMcpVersion() {
		return mcpVersion;
	}

	public void setMcpVersion(String mcpVersion) {
		this.mcpVersion = mcpVersion;
	}

	public long getMessageId() {
		return messageId;
	}

	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}

	public String getControllerSerialNumber() {
		return controllerSerialNumber;
	}

	public void setControllerSerialNumber(String controllerSerialNumber) {
		this.controllerSerialNumber = controllerSerialNumber;
	}

	public long getVisitId() {
		return visitId;
	}

	public void setVisitId(long visitId) {
		this.visitId = visitId;
	}

	public int getSystemCycleCount() {
		return systemCycleCount;
	}

	public void setSystemCycleCount(int sysCycleCount) {
		this.systemCycleCount = sysCycleCount;
	}

	public int getControllerCycleCount() {
		return controllerCycleCount;
	}

	public void setControllerCycleCount(int controllerCycleCount) {
		this.controllerCycleCount = controllerCycleCount;
	}

	public int getRssiSignalStrength() {
		return rssiSignalStrength;
	}

	public void setRssiSignalStrength(int rssiSignalStrength) {
		this.rssiSignalStrength = rssiSignalStrength;
	}


	public String getHealthCd() {
		return healthCd;
	}

	public void setHealthCd(String healthCd) {
		this.healthCd = healthCd;
	}

	public Date getInsertTs() {
		return insertTs;
	}

	public void setInsertTS(Date insertTs) {
		this.insertTs = insertTs;
	}

	public Date getUpdateTS() {
		return updateTs;
	}

	public void setUpdateTS(Date updateTs) {
		this.updateTs = updateTs;
	}
	
	public String toString() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		StringBuilder sb = new StringBuilder();
		sb.append("doorID: ").append(doorId);
		sb.append("\nmessageID: ").append(messageId);
		sb.append("\nlcpVersion").append(lcpVersion);
		sb.append("\nmcpVersion").append(mcpVersion);
		sb.append("\ncontrollerCycleCount: ").append(Integer.toString(controllerCycleCount));
		sb.append("\nsystemCycleCount: ").append(Double.toString(systemCycleCount));
		sb.append("\nrssiSignalStrength: ").append(Integer.toString(rssiSignalStrength));
		sb.append("\ndoorHealthCode: ").append(healthCd);
		return sb.toString();
	}
	
}
