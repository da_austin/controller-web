package com.sbdinc.da.smartdoor.dcweb.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
/**
 * 
 * Entity class for Message
 *
 * @author Dian Jiao
 *
 */
@Entity
@Table(name = "MSG_RCV")
public class Message implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "MSG_ID")
    private long id;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MSG_RCPT_DTM")
	private Date msgRecvDt;
	
	@Column(name = "DATA_SRC_CD")
	private String sourceCode;
	
	@Column(name = "MSG_TYPE")
	private String type;
	
	@Column(name = "MSG_FMT")
	private String format;
	
	@Column(name = "POINT_OF_TRIGGER")
	private String trigger;
	
	@Column(name = "MSG_BODY")
	private String msgBody;	
	
	@Column(name = "DATA_LOAD_STATUS", insertable=false)
	private char loadStatus;
	
	@Column(name = "NOTE", insertable=false)
	private String note;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Date getMsgRecvDt() {
		return msgRecvDt;
	}

	public void setMsgRecvDt(Date msgRecvDt) {
		this.msgRecvDt = msgRecvDt;
	}

	public String getSourceCode() {
		return sourceCode;
	}

	public void setSourceCode(String sourceCode) {
		this.sourceCode = sourceCode;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getTrigger() {
		return trigger;
	}

	public void setTrigger(String trigger) {
		this.trigger = trigger;
	}

	public String getMsgBody() {
		return msgBody;
	}

	public void setMsgBody(String msgBody) {
		this.msgBody = msgBody;
	}
	
	
	public char getLoadStatus() {
		return loadStatus;
	}

	public void setLoadStatus(char loadStatus) {
		this.loadStatus = loadStatus;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public String toString() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		StringBuilder sb = new StringBuilder();
		sb.append("msgID: ").append(id);
		sb.append("\nmsgRecvDtm: ").append(df.format(msgRecvDt));
		sb.append("\nmsgType: ").append(type);
		sb.append("\nmsgFormat: ").append(format);
		sb.append("\npointOfTrigger: ").append(trigger);
		sb.append("\nloadStatus: ").append(loadStatus);
		return sb.toString();
	}
}
