package com.sbdinc.da.smartdoor.dcweb.model;

import java.math.BigDecimal;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * Value Object  for API JSON input
 *
 * @author Dian Jiao
 *
 */
public class MobileVO {
	
	// Controller variables
	@NotNull
	@NotBlank
	private String activeDoorId;
	private String activeControllerSerialNumber;
	private String controllerType;
	private String lcpVersion;
	private String mcpVersion;
	public String getLcpVersion() {
		return lcpVersion;
	}

	public void setLcpVersion(String lcpVersion) {
		this.lcpVersion = lcpVersion;
	}

	public String getMcpVersion() {
		return mcpVersion;
	}

	public void setMcpVersion(String mcpVersion) {
		this.mcpVersion = mcpVersion;
	}

	private String originalControllerSerialNumber;
	private String originalDoorId;
	
	// Technician Visit variables
	@NotNull
	@NotBlank
	private String androidId;
	
	private int apiLevel;
	private String emailAddress;
	private String readingDateTime;
	private int readingTimeZone;
	private BigDecimal lattitude;
	private BigDecimal longitude;
	private String doorHealthCode;
	private String macAddress;
	private String phoneNumber;
	private BigDecimal appVersion;
	private String appDbVersion;
	private String doorIdPopulateMethod;
	
	// Cycle count variables
	private int systemCycleCount;
	
	@NotNull
	@Min(0)
	private int controllerCycleCount;
	
	// wifi signal strength
	private int rssiSignalStrength;

	public String getActiveDoorId() {
		return activeDoorId;
	}

	public void setActiveDoorId(String activeDoorId) {
		this.activeDoorId = activeDoorId;
	}

	public String getActiveControllerSerialNumber() {
		return activeControllerSerialNumber;
	}

	public void setActiveControllerSerialNumber(String activeControllerSerialNumber) {
		this.activeControllerSerialNumber = activeControllerSerialNumber;
	}

	public String getControllerType() {
		return controllerType;
	}

	public void setControllerType(String controllerType) {
		this.controllerType = controllerType;
	}

	public String getOriginalControllerSerialNumber() {
		return originalControllerSerialNumber;
	}

	public void setOriginalControllerSerialNumber(String originalControllerSerialNumber) {
		this.originalControllerSerialNumber = originalControllerSerialNumber;
	}

	public String getOriginalDoorId() {
		return originalDoorId;
	}

	public void setOriginalDoorId(String originalDoorId) {
		this.originalDoorId = originalDoorId;
	}

	public String getAndroidId() {
		return androidId;
	}

	public void setAndroidId(String androidId) {
		this.androidId = androidId;
	}

	public int getApiLevel() {
		return apiLevel;
	}

	public void setApiLevel(int apiLevel) {
		this.apiLevel = apiLevel;
	}
	
	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getMacAddress() {
		return macAddress;
	}

	public void setMacAddress(String macAddress) {
		this.macAddress = macAddress;
	}

	public String getReadingDateTime() {
		return readingDateTime;
	}

	public void setReadingDateTime(String readingDateTime) {
		this.readingDateTime = readingDateTime;
	}

	public int getReadingTimeZone() {
		return readingTimeZone;
	}

	public void setReadingTimeZone(int readingTimeZone) {
		this.readingTimeZone = readingTimeZone;
	}

	public BigDecimal getLattitude() {
		return lattitude;
	}

	public void setLattitude(BigDecimal lattitude) {
		this.lattitude = lattitude;
	}

	public BigDecimal getLongitude() {
		return longitude;
	}

	public void setLongitude(BigDecimal longitude) {
		this.longitude = longitude;
	}

	public String getDoorHealthCode() {
		return doorHealthCode;
	}

	public void setDoorHealthCode(String doorHealthCode) {
		this.doorHealthCode = doorHealthCode;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public BigDecimal getAppVersion() {
		return appVersion;
	}

	public void setAppVersion(BigDecimal appVersion) {
		this.appVersion = appVersion;
	}

	public int getSystemCycleCount() {
		return systemCycleCount;
	}

	public void setSystemCycleCount(int systemCycleCount) {
		this.systemCycleCount = systemCycleCount;
	}

	public int getControllerCycleCount() {
		return controllerCycleCount;
	}

	public void setControllerCycleCount(int controllerCycleCount) {
		this.controllerCycleCount = controllerCycleCount;
	}
	
	public int getRssiSignalStrength() {
		return rssiSignalStrength;
	}

	public void setRssiSignalStrength(int rssiSignalStrength) {
		this.rssiSignalStrength = rssiSignalStrength;
	}

	public String getAppDbVersion() {
		return appDbVersion;
	}

	public void setAppDbVersion(String appDbVersion) {
		this.appDbVersion = appDbVersion;
	}

	public String getDoorIdPopulateMethod() {
		return doorIdPopulateMethod;
	}

	public void setDoorIdPopulateMethod(String doorIdPopulateMethod) {
		this.doorIdPopulateMethod = doorIdPopulateMethod;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("activeControllerSerialNumber: ").append(activeControllerSerialNumber).append(",\n");
		sb.append("activeDoorId: ").append(activeDoorId).append(",\n");
		sb.append("controllerType: ").append(controllerType).append(",\n");
		sb.append("lcpVersion: ").append(lcpVersion).append(",\n");
		sb.append("mcpVersion: ").append(mcpVersion).append(",\n");
		sb.append("originalControllerSerialNumber: ").append(originalControllerSerialNumber).append(",\n");
		sb.append("originalDoorId: ").append(originalDoorId).append(",\n");
		sb.append("androidId: ").append(androidId).append(",\n");
		sb.append("apiLevel: ").append(apiLevel).append(",\n");
		sb.append("emailAddress: ").append(emailAddress).append(",\n");
		sb.append("readingDateTime: ").append(readingDateTime).append(",\n");
		
		sb.append("readingTimeZone: ").append(Integer.toString(readingTimeZone)).append(",\n");
		sb.append("lattitude: ").append(lattitude).append(",\n");
		sb.append("longitude: ").append(longitude).append(",\n");;
		sb.append("doorHealthCode: ").append(doorHealthCode).append(",\n");;
		sb.append("phoneNumber: ").append(phoneNumber).append(",\n");
		sb.append("appVersion: ").append(appVersion).append(",\n");
		sb.append("appDbVersion: ").append(appDbVersion).append(",\n");
		sb.append("macAddress: ").append(macAddress).append(",\n");
		sb.append("systemCycleCount: ").append(systemCycleCount).append(",\n");
		sb.append("controllerCycleCount: ").append(controllerCycleCount).append(",\n");
		sb.append("rssiSignalStrength: ").append(rssiSignalStrength).append(",\n");
		sb.append("doorIdPopulateMethod: ").append(doorIdPopulateMethod).append(",\n");
		return sb.toString();
	}
}
