package com.sbdinc.da.smartdoor.dcweb.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entity class for Controller Settings
 * 
 * @author Dian Jiao
 *
 */
@Entity
@Table(name = "CTRLR_SETNG_HIST")
public class Setting implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "SETNG_ID")
    private long id;
	
	@Column(name = "MSG_ID")
	private long messageId;
	
	@Column(name = "VISIT_ID")
	private long visitId;
	
	@Column(name = "CTRLR_SERL_NBR")
	private String controllerSerialNumber;
	
	@Column(name = "DOOR_ID")
	private String doorId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "FIRST_READ_DTM")
	private Date firstReadDt;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_READ_DTM")
	private Date lastReadDt;
	
	@Column(name = "CTRLR_TIMEZONE")
	private int timezone;
	
	@Column(name = "OPER_CD")
	private String opCode;
	
	@Column(name = "M1_HANDING_CODE")
	private String m1Handing;	
	@Column(name = "M2_HANDING_CODE")
	private String m2Handing;
	
	@Column(name = "M1_ENCODER_TYP")
	private String m1Encoder;
	@Column(name = "M2_ENCODER_TYP")
	private String m2Encoder;
	
	@Column(name = "FIRMWARE_LOGIC_VER")
	private String fwLogicVer;
	
	@Column(name = "FIRMWARE_MOTION_VER")
	private String fwMotionVer;

	@Column(name = "M1_OPN_SPEED")
	private int m1OpenSpeed;
	@Column(name = "M2_OPN_SPEED")
	private int m2OpenSpeed;
	
	@Column(name = "M1_OPN_TORQUE")
	private int m1OpenTorque;
	@Column(name = "M2_OPN_TORQUE")
	private int m2OpenTorque;
	
	@Column(name = "M1_OPN_ACCL")
	private int m1OpenAcceleration;
	@Column(name = "M2_OPN_ACCL")
	private int m2OpenAcceleration;
		
	@Column(name = "M1_CHK_SPEED")
	private int m1CheckSpeed;
	@Column(name = "M2_CHK_SPEED")
	private int m2CheckSpeed;
	
	@Column(name = "M1_CHK_TORQUE")
	private int m1CheckTorque;
	@Column(name = "M2_CHK_TORQUE")
	private int m2CheckTorque;
	
	@Column(name = "M1_DR_OBS_TM")
	private int m1DoorObstructionTime;
	@Column(name = "M2_DR_OBS_TM")
	private int m2DoorObstructionTime;
	
	@Column(name = "M1_OPN_OBS_TM")
	private int m1OpenObstructionTime;
	@Column(name = "M2_OPN_OBS_TM")
	private int m2OpenObstructionTime;
	
	@Column(name = "M1_OPN_BRAKING")
	private int m1OpenBraking;
	@Column(name = "M2_OPN_BRAKING")
	private int m2OpenBraking;
	
	@Column(name = "M1_OPN_CHK_SPEED")
	private int m1OpenCheckSpeed;
	@Column(name = "M2_OPN_CHK_SPEED")
	private int m2OpenCheckSpeed;
	
	@Column(name = "M1_OPN_CHK_TORQUE")
	private int m1OpenCheckTorque;
	@Column(name = "M2_OPN_CHK_TORQUE")
	private int m2OpenCheckTorque;
	
	@Column(name = "M1_OPN_CHK_PCT")
	private int m1OpenCheckPercent;
	@Column(name = "M2_OPN_CHK_PCT")
	private int m2OpenCheckPercent;
	
	@Column(name = "M1_OPN_CHK_OBS_TM")
	private int m1OpenCheckObstructionTime;
	@Column(name = "M2_OPN_CHK_OBS_TM")
	private int m2OpenCheckObstructionTime;
	
	@Column(name = "M1_OPN_LRN_SPEED")
	private int m1OpenLearnSpeed;
	@Column(name = "M2_OPN_LRN_SPEED")
	private int m2OpenLearnSpeed;
	
	@Column(name = "M1_OPN_LRN_TORQUE")
	private int m1OpenLearnTorque;
	@Column(name = "M2_OPN_LRN_TORQUE")
	private int m2OpenLearnTorque;
	
	@Column(name = "M1_OPN_STARTUP_TORQUE")
	private int m1OpenStartupTorque;
	@Column(name = "M2_OPN_STARTUP_TORQUE")
	private int m2OpenStartupTorque;
	
	@Column(name = "M1_OPN_STARTUP_LEN")
	private int m1OpenStartupLen;
	@Column(name = "M2_OPN_STARTUP_LEN")
	private int m2OpenStartupLen;	

	@Column(name = "M1_OPN_STARTUP_OBS_TM")
	private int m1OpenStartupObsTime;
	@Column(name = "M2_OPN_STARTUP_OBS_TM")
	private int m2OpenStartupObsTime;
	
	@Column(name = "M1_OPN_CHK_LEN")
	private int m1OpenCheckLen;
	@Column(name = "M2_OPN_CHK_LEN")
	private int m2OpenCheckLen;
	
	@Column(name = "M1_OPN_CHK_BOOST")
	private int m1OpenCheckBoost;
	@Column(name = "M2_OPN_CHK_BOOST")
	private int m2OpenCheckBoost;
	
	@Column(name = "M1_HOLD_OPN_DELAY")
	private int m1HoldOpenDelay;
	@Column(name = "M2_HOLD_OPN_DELAY")
	private int m2HoldOpenDelay;
	
	@Column(name = "M1_CLOSE_CHK_OBS_TM")
	private int m1CloseCheckObsTime;
	@Column(name = "M2_CLOSE_CHK_OBS_TM")
	private int m2CloseCheckObsTime;
	
	@Column(name = "M1_CLOSE_OBS_TM")
	private int m1CloseObsTime;
	@Column(name = "M2_CLOSE_OBS_TM")
	private int m2CloseObsTime;
	
	@Column(name = "SFTY_LOGIC")
	private int safetyLogic;
	
	@Column(name = "HOLD_BEAM_TYP")
	private int holdBeamType;
	
	@Column(name = "LOCK_LOGIC")
	private int lockLogic;
	
	@Column(name = "TWOS_OPER")
	private int twos;
	
	@Column(name = "LOCK_RELS_TORQUE")
	private int lockReleaseTorque;
	
	@Column(name = "LOCK_RELEASE_TM")
	private int lockReleaseTime;
	
	@Column(name = "LOCK_DELAY")
	private int lockDelay;
	
	@Column(name = "M1_RECYCLE_SPEED")
	private int m1RecycleSpeed;
	@Column(name = "M2_RECYCLE_SPEED")
	private int m2RecycleSpeed;
	
	@Column(name = "M1_OPN_STOP")
	private int m1OpenStop;
	@Column(name = "M2_OPN_STOP")
	private int m2OpenStop;
	
	@Column(name = "REDUCED_OPN_LEN")
	private int reducedOpenLen;
	
	@Column(name = "CLOSE_LEARN_SPEED")
	private int closeLearnSpeed;
	
	@Column(name = "CLOSE_LEARN_TORQUE")
	private int closeLearnTorque;
	
	@Column(name = "CLOSE_STARTUP_TORQUE")
	private int closeStartupTorque;
	
	@Column(name = "CLOSE_STARTUP_LEN")
	private int closeStartupLen;
	
	@Column(name = "CLOSE_SPEED")
	private int closeSpeed;
	
	@Column(name = "CLOSE_TORQUE")
	private int closeTorque;
	
	@Column(name = "CLOSE_ACCL")
	private int closeAccl;
	
	@Column(name = "CLOSE_CHK_PCT")
	private int closeCheckPct;
	
	@Column(name = "CLOSE_CHK_SPEED")
	private int closeCheckSpeed;
	
	@Column(name = "CLOSE_CHK_TORQUE")
	private int closeCheckTorque;
	
	@Column(name = "CLOSE_BRAKING")
	private int closeBraking;
	
	@Column(name = "CLOSE_PRESS")
	private int closePress;
	
	@Column(name = "FUNC_SWITCH_TYP")
	private int funcSwitchType;

	@Column(name = "LOCK_TM")
	private int lockTime;
	
	@Column(name = "DELAYED_EGRESS")
	private int delayedEgress;
	
	@Column(name = "ACCESS_CTRL_PRO")
	private int accessCtrlPro;
	
	@Column(name = "IO_CONFIG")
	private int ioConfig;
	
	@Column(name = "FIRE_ALARM_MD")
	private int fireAlarmMode;
	
	@Column(name = "M1_FULL_OPN_OFFSET")
	private int m1FullOpenOffset;
	@Column(name = "M2_FULL_OPN_OFFSET")
	private int m2FullOpenOffset;
	
	@Column(name = "M1_FULL_OPN_TORQUE")
	private int m1FullOpenTorque;
	@Column(name = "M2_FULL_OPN_TORQUE")
	private int m2FullOpenTorque;
	
	@Column(name = "M1_MT_SENSITIVITY")
	private int m1MtSensitivity;
	@Column(name = "M2_MT_SENSITIVITY")
	private int m2MtSensitivity;
	
	@Column(name = "M1_MT_HOLD_OPN_TM")
	private int m1MtHoldOpenTime;
	@Column(name = "M2_MT_HOLD_OPN_TM")
	private int m2MtHoldOpenTime;
	
	@Column(name = "M1_CLOSE_CHK_LEN")
	private int m1CloseCheckLen;
	@Column(name = "M2_CLOSE_CHK_LEN")
	private int m2CloseCheckLen;
	
	@Column(name = "M1_CLOSE_CHK_BOOST")
	private int m1CloseCheckBoost;
	@Column(name = "M2_CLOSE_CHK_BOOST")
	private int m2CloseCheckBoost;
	
	@Column(name = "M1_PWR_CLOSE_SPD")
	private int m1PowerCloseSpeed;
	@Column(name = "M2_PWR_CLOSE_SPD")
	private int m2PowerCloseSpeed;
	
	@Column(name = "M1_PWR_CLOSE_TORQUE")
	private int m1PowerCloseTorque;
	@Column(name = "M2_PWR_CLOSE_TORQUE")
	private int m2PowerCloseTorque;
	
	@Column(name = "M1_PWR_CLOSE_OBS_TM")
	private int m1PowerCloseObsTime;
	@Column(name = "M2_PWR_CLOSE_OBS_TM")
	private int m2PowerCloseObsTime;
	
	@Column(name = "M1_PWR_CLOSE_LEN")
	private int m1PowerCloseLen;
	@Column(name = "M2_PWR_CLOSE_LEN")
	private int m2PowerCloseLen;
	
	@Column(name = "M1_PWR_CLOSE_TM")
	private int m1PowerCloseTm;
	@Column(name = "M2_PWR_CLOSE_TM")
	private int m2PowerCloseTm;
	
	@Column(name = "M1_STALL_SPD")
	private int m1StallSpeed;
	@Column(name = "M2_STALL_SPD")
	private int m2StallSpeed;
	
	@Column(name = "M1_STALL_TORQUE")
	private int m1StallTorque;
	@Column(name = "M2_STALL_TORQUE")
	private int m2StallTorque;
	
	@Column(name = "M1_OP_DELAY")
	private int m1OpDelay;
	@Column(name = "M2_OP_DELAY")
	private int m2OpDelay;
	
	@Column(name = "M1_DOOR_TRVL")
	private int m1DoorTravel;
	@Column(name = "M2_DOOR_TRVL")
	private int m2DoorTravel;
	
	@Column(name = "OVSPD_COMP_RATE")
	private int overspeedCompRate;
	
	@Column(name = "OFF_MODE")
	private int offMode;
		
	@Column(name = "UNLOCK_TM")
	private int unlockTime;
	
	@Column(name = "SENSOR_OVERRIDE")
	private int sensorOverride;
		
	@Column(name = "NO_REV_OBS")
	private int noRevObs;
	
	@Column(name = "CLOSE_WO_OBSTRUCTION")
	private int closeWoOb;

	@Column(name = "OPTION_THREE")
	private int optionThree;
	
	@Column(name = "OPTION_FOUR")
	private int optionFour;
	
	@Column(name = "OPN_PWR_ASST_TORQUE")
	private int openPowerAssistTorque;
	
	@Column(name = "CLOSE_PWR_ASST_TORQUE")
	private int closePowerAssistTorque;
	
	@Column(name = "M1_OPN_LRN_OBS_TM")
	private int m1OpenLearnObstructionTime;
	@Column(name = "M2_OPN_LRN_OBS_TM")
	private int m2OpenLearnObstructionTime;
	
	@Column(name = "M1_OPN_SU_OBS_TM")
	private int m1OpenStartupObstructionTime;
	@Column(name = "M2_OPN_SU_OBS_TM")
	private int m2OpenStartupObstructionTime;
	
	@Column(name = "M1_PWR_ASST_TORQUE")
	private int m1PowerAssistTorque;
	@Column(name = "M2_PWR_ASST_TORQUE")
	private int m2PowerAssistTorque;
	
	@Column(name = "M1_PWR_ASST_EXIT_ANGLE")
	private int m1PowerAssistExitAngle;
	@Column(name = "M2_PWR_ASST_EXIT_ANGLE")
	private int m2PowerAssistExitAngle;
	
	@Column(name = "M1_PWR_ASST_TRIG_THOLD")
	private int m1PowerAssistTriggerThreshold;
	@Column(name = "M2_PWR_ASST_TRIG_THOLD")
	private int m2PowerAssistTriggerThreshold;
	
	@Column(name = "M1_STALL_SNSR_INPUT")
	private int m1StallSensorInput;
	@Column(name = "M2_STALL_SNSR_INPUT")
	private int m2StallSensorInput;
	
	@Column(name = "M1_MAGIC_TOUCH_FEEL")
	private int m1MagicTouchFeel;
	@Column(name = "M2_MAGIC_TOUCH_FEEL")
	private int m2MagicTouchFeel;
	
	@Column(name = "M1_MOTOR_START_SPD")
	private int m1MotorStartSpeed;
	@Column(name = "M2_MOTOR_START_SPD")
	private int m2MotorStartSpeed;
	
	@Column(name = "M1_CREEP_LEN")
	private int m1CreepLength;
	@Column(name = "M2_CREEP_LEN")
	private int m2CreepLength;
	
	@Column(name = "M1_PRELOAD_TRQ")
	private int m1PreloadTorque;
	@Column(name = "M2_PRELOAD_TRQ")
	private int m2PreloadTorque;
	
	@Column(name = "M1_PRELOAD_MD")
	private int m1PreloadMode;
	@Column(name = "M2_PRELOAD_MD")
	private int m2PreloadMode;
	
	@Column(name = "M1_PWR_ASSIST_SENS")
	private int m1PowerAssistSensitivity;
	@Column(name = "M2_PWR_ASSIST_SENS")
	private int m2PowerAssistSensitivity;
	
	@Column(name = "M1_CREEP_SPD")
	private int m1CreepSpeed;
	@Column(name = "M2_CREEP_SPD")
	private int m2CreepSpeed;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ETL_INSERT_TS", insertable=true, updatable=false)
	private Date insertTS;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ETL_UPDATE_TS", insertable=false, updatable=true)
	private Date updateTS;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getDoorId() {
		return doorId;
	}

	public void setDoorId(String doorId) {
		this.doorId = doorId;
	}

	public long getMessageId() {
		return messageId;
	}

	public void setMessageId(long messageId) {
		this.messageId = messageId;
	}

	public long getVisitId() {
		return visitId;
	}

	public void setVisitId(long visitId) {
		this.visitId = visitId;
	}

	public String getControllerSerialNumber() {
		return controllerSerialNumber;
	}

	public void setControllerSerialNumber(String controllerSerialNumber) {
		this.controllerSerialNumber = controllerSerialNumber;
	}

	public Date getFirstReadDt() {
		return firstReadDt;
	}

	public void setFirstReadDt(Date firstReadDt) {
		this.firstReadDt = firstReadDt;
	}

	public Date getLastReadDt() {
		return lastReadDt;
	}

	public void setLastReadDt(Date lastReadDt) {
		this.lastReadDt = lastReadDt;
	}

	public int getTimezone() {
		return timezone;
	}

	public void setTimezone(int timezone) {
		this.timezone = timezone;
	}

	public String getOpCode() {
		return opCode;
	}

	public void setOpCode(String opCode) {
		this.opCode = opCode;
	}

	public String getM1Handing() {
		return m1Handing;
	}

	public void setM1Handing(String m1Handing) {
		this.m1Handing = m1Handing;
	}

	public String getM2Handing() {
		return m2Handing;
	}

	public void setM2Handing(String m2Handing) {
		this.m2Handing = m2Handing;
	}

	public String getM1Encoder() {
		return m1Encoder;
	}

	public void setM1Encoder(String m1Encoder) {
		this.m1Encoder = m1Encoder;
	}

	public String getM2Encoder() {
		return m2Encoder;
	}

	public void setM2Encoder(String m2Encoder) {
		this.m2Encoder = m2Encoder;
	}

	public String getFwLogicVer() {
		return fwLogicVer;
	}

	public void setFwLogicVer(String fwLogicVer) {
		this.fwLogicVer = fwLogicVer;
	}

	public String getFwMotionVer() {
		return fwMotionVer;
	}

	public void setFwMotionVer(String fwMotionVer) {
		this.fwMotionVer = fwMotionVer;
	}

	public int getM1OpenSpeed() {
		return m1OpenSpeed;
	}

	public void setM1OpenSpeed(int m1OpenSpeed) {
		this.m1OpenSpeed = m1OpenSpeed;
	}

	public int getM2OpenSpeed() {
		return m2OpenSpeed;
	}

	public void setM2OpenSpeed(int m2OpenSpeed) {
		this.m2OpenSpeed = m2OpenSpeed;
	}

	public int getM1OpenTorque() {
		return m1OpenTorque;
	}

	public void setM1OpenTorque(int m1OpenTorque) {
		this.m1OpenTorque = m1OpenTorque;
	}

	public int getM2OpenTorque() {
		return m2OpenTorque;
	}

	public void setM2OpenTorque(int m2OpenTorque) {
		this.m2OpenTorque = m2OpenTorque;
	}

	public int getM1OpenAcceleration() {
		return m1OpenAcceleration;
	}

	public void setM1OpenAcceleration(int m1OpenAcceleration) {
		this.m1OpenAcceleration = m1OpenAcceleration;
	}

	public int getM2OpenAcceleration() {
		return m2OpenAcceleration;
	}

	public void setM2OpenAcceleration(int m2OpenAcceleration) {
		this.m2OpenAcceleration = m2OpenAcceleration;
	}

	public int getM1CheckSpeed() {
		return m1CheckSpeed;
	}

	public void setM1CheckSpeed(int m1CheckSpeed) {
		this.m1CheckSpeed = m1CheckSpeed;
	}

	public int getM2CheckSpeed() {
		return m2CheckSpeed;
	}

	public void setM2CheckSpeed(int m2CheckSpeed) {
		this.m2CheckSpeed = m2CheckSpeed;
	}

	public int getM1CheckTorque() {
		return m1CheckTorque;
	}

	public void setM1CheckTorque(int m1CheckTorque) {
		this.m1CheckTorque = m1CheckTorque;
	}

	public int getM2CheckTorque() {
		return m2CheckTorque;
	}

	public void setM2CheckTorque(int m2CheckTorque) {
		this.m2CheckTorque = m2CheckTorque;
	}

	public int getM1DoorObstructionTime() {
		return m1DoorObstructionTime;
	}

	public void setM1DoorObstructionTime(int m1DoorObstructionTime) {
		this.m1DoorObstructionTime = m1DoorObstructionTime;
	}

	public int getM2DoorObstructionTime() {
		return m2DoorObstructionTime;
	}

	public void setM2DoorObstructionTime(int m2DoorObstructionTime) {
		this.m2DoorObstructionTime = m2DoorObstructionTime;
	}

	public int getM1OpenObstructionTime() {
		return m1OpenObstructionTime;
	}

	public void setM1OpenObstructionTime(int m1OpenObstructionTime) {
		this.m1OpenObstructionTime = m1OpenObstructionTime;
	}

	public int getM2OpenObstructionTime() {
		return m2OpenObstructionTime;
	}

	public void setM2OpenObstructionTime(int m2OpenObstructionTime) {
		this.m2OpenObstructionTime = m2OpenObstructionTime;
	}

	public int getM1OpenBraking() {
		return m1OpenBraking;
	}

	public void setM1OpenBraking(int m1OpenBraking) {
		this.m1OpenBraking = m1OpenBraking;
	}

	public int getM2OpenBraking() {
		return m2OpenBraking;
	}

	public void setM2OpenBraking(int m2OpenBraking) {
		this.m2OpenBraking = m2OpenBraking;
	}

	public int getM1OpenCheckSpeed() {
		return m1OpenCheckSpeed;
	}

	public void setM1OpenCheckSpeed(int m1OpenCheckSpeed) {
		this.m1OpenCheckSpeed = m1OpenCheckSpeed;
	}

	public int getM2OpenCheckSpeed() {
		return m2OpenCheckSpeed;
	}

	public void setM2OpenCheckSpeed(int m2OpenCheckSpeed) {
		this.m2OpenCheckSpeed = m2OpenCheckSpeed;
	}

	public int getM1OpenCheckTorque() {
		return m1OpenCheckTorque;
	}

	public void setM1OpenCheckTorque(int m1OpenCheckTorque) {
		this.m1OpenCheckTorque = m1OpenCheckTorque;
	}

	public int getM2OpenCheckTorque() {
		return m2OpenCheckTorque;
	}

	public void setM2OpenCheckTorque(int m2OpenCheckTorque) {
		this.m2OpenCheckTorque = m2OpenCheckTorque;
	}

	public int getM1OpenCheckPercent() {
		return m1OpenCheckPercent;
	}

	public void setM1OpenCheckPercent(int m1OpenCheckPercent) {
		this.m1OpenCheckPercent = m1OpenCheckPercent;
	}

	public int getM2OpenCheckPercent() {
		return m2OpenCheckPercent;
	}

	public void setM2OpenCheckPercent(int m2OpenCheckPercent) {
		this.m2OpenCheckPercent = m2OpenCheckPercent;
	}

	public int getM1OpenCheckObstructionTime() {
		return m1OpenCheckObstructionTime;
	}

	public void setM1OpenCheckObstructionTime(int m1OpenCheckObstructionTime) {
		this.m1OpenCheckObstructionTime = m1OpenCheckObstructionTime;
	}

	public int getM2OpenCheckObstructionTime() {
		return m2OpenCheckObstructionTime;
	}

	public void setM2OpenCheckObstructionTime(int m2OpenCheckObstructionTime) {
		this.m2OpenCheckObstructionTime = m2OpenCheckObstructionTime;
	}

	public int getM1OpenLearnSpeed() {
		return m1OpenLearnSpeed;
	}

	public void setM1OpenLearnSpeed(int m1OpenLearnSpeed) {
		this.m1OpenLearnSpeed = m1OpenLearnSpeed;
	}

	public int getM2OpenLearnSpeed() {
		return m2OpenLearnSpeed;
	}

	public void setM2OpenLearnSpeed(int m2OpenLearnSpeed) {
		this.m2OpenLearnSpeed = m2OpenLearnSpeed;
	}

	public int getM1OpenLearnTorque() {
		return m1OpenLearnTorque;
	}

	public void setM1OpenLearnTorque(int m1OpenLearnTorque) {
		this.m1OpenLearnTorque = m1OpenLearnTorque;
	}

	public int getM2OpenLearnTorque() {
		return m2OpenLearnTorque;
	}

	public void setM2OpenLearnTorque(int m2OpenLearnTorque) {
		this.m2OpenLearnTorque = m2OpenLearnTorque;
	}

	public int getM1OpenStartupTorque() {
		return m1OpenStartupTorque;
	}

	public void setM1OpenStartupTorque(int m1OpenStartupTorque) {
		this.m1OpenStartupTorque = m1OpenStartupTorque;
	}

	public int getM2OpenStartupTorque() {
		return m2OpenStartupTorque;
	}

	public void setM2OpenStartupTorque(int m2OpenStartupTorque) {
		this.m2OpenStartupTorque = m2OpenStartupTorque;
	}

	
	public int getM1OpenStartupObsTime() {
		return m1OpenStartupObsTime;
	}

	public void setM1OpenStartupObsTime(int m1OpenStartupObsTime) {
		this.m1OpenStartupObsTime = m1OpenStartupObsTime;
	}

	public int getM2OpenStartupObsTime() {
		return m2OpenStartupObsTime;
	}

	public void setM2OpenStartupObsTime(int m2OpenStartupObsTime) {
		this.m2OpenStartupObsTime = m2OpenStartupObsTime;
	}

	public int getM1OpenStartupLen() {
		return m1OpenStartupLen;
	}

	public void setM1OpenStartupLen(int m1OpenStartupLen) {
		this.m1OpenStartupLen = m1OpenStartupLen;
	}

	public int getM2OpenStartupLen() {
		return m2OpenStartupLen;
	}

	public void setM2OpenStartupLen(int m2OpenStartupLen) {
		this.m2OpenStartupLen = m2OpenStartupLen;
	}

	public int getM1OpenCheckLen() {
		return m1OpenCheckLen;
	}

	public void setM1OpenCheckLen(int m1OpenCheckLen) {
		this.m1OpenCheckLen = m1OpenCheckLen;
	}

	public int getM2OpenCheckLen() {
		return m2OpenCheckLen;
	}

	public void setM2OpenCheckLen(int m2OpenCheckLen) {
		this.m2OpenCheckLen = m2OpenCheckLen;
	}

	public int getM1HoldOpenDelay() {
		return m1HoldOpenDelay;
	}

	public void setM1HoldOpenDelay(int m1HoldOpenDelay) {
		this.m1HoldOpenDelay = m1HoldOpenDelay;
	}

	public int getM2HoldOpenDelay() {
		return m2HoldOpenDelay;
	}

	public void setM2HoldOpenDelay(int m2HoldOpenDelay) {
		this.m2HoldOpenDelay = m2HoldOpenDelay;
	}

	public int getM1CloseCheckObsTime() {
		return m1CloseCheckObsTime;
	}

	public void setM1CloseCheckObsTime(int m1CloseCheckObsTime) {
		this.m1CloseCheckObsTime = m1CloseCheckObsTime;
	}

	public int getM2CloseCheckObsTime() {
		return m2CloseCheckObsTime;
	}

	public void setM2CloseCheckObsTime(int m2CloseCheckObsTime) {
		this.m2CloseCheckObsTime = m2CloseCheckObsTime;
	}

	public int getSafetyLogic() {
		return safetyLogic;
	}

	public void setSafetyLogic(int safetyLogic) {
		this.safetyLogic = safetyLogic;
	}

	public int getHoldBeamType() {
		return holdBeamType;
	}

	public void setHoldBeamType(int holdBeamType) {
		this.holdBeamType = holdBeamType;
	}

	public int getLockLogic() {
		return lockLogic;
	}

	public void setLockLogic(int lockLogic) {
		this.lockLogic = lockLogic;
	}

	public int getTwos() {
		return twos;
	}

	public void setTwos(int twos) {
		this.twos = twos;
	}

	public int getLockReleaseTorque() {
		return lockReleaseTorque;
	}

	public void setLockReleaseTorque(int lockReleaseTorque) {
		this.lockReleaseTorque = lockReleaseTorque;
	}

	public int getLockReleaseTime() {
		return lockReleaseTime;
	}

	public void setLockReleaseTime(int lockReleaseTime) {
		this.lockReleaseTime = lockReleaseTime;
	}

	public int getLockDelay() {
		return lockDelay;
	}

	public void setLockDelay(int lockDelay) {
		this.lockDelay = lockDelay;
	}

	public int getReducedOpenLen() {
		return reducedOpenLen;
	}

	public void setReducedOpenLen(int reducedOpenLen) {
		this.reducedOpenLen = reducedOpenLen;
	}

	public int getCloseLearnSpeed() {
		return closeLearnSpeed;
	}

	public void setCloseLearnSpeed(int closeLearnSpeed) {
		this.closeLearnSpeed = closeLearnSpeed;
	}

	public int getCloseLearnTorque() {
		return closeLearnTorque;
	}

	public void setCloseLearnTorque(int closeLearnTorque) {
		this.closeLearnTorque = closeLearnTorque;
	}

	public int getCloseStartupTorque() {
		return closeStartupTorque;
	}

	public void setCloseStartupTorque(int closeStartupTorque) {
		this.closeStartupTorque = closeStartupTorque;
	}

	public int getCloseStartupLen() {
		return closeStartupLen;
	}

	public void setCloseStartupLen(int closeStartupLen) {
		this.closeStartupLen = closeStartupLen;
	}

	public int getCloseSpeed() {
		return closeSpeed;
	}

	public void setCloseSpeed(int closeSpeed) {
		this.closeSpeed = closeSpeed;
	}

	public int getCloseTorque() {
		return closeTorque;
	}

	public void setCloseTorque(int closeTorque) {
		this.closeTorque = closeTorque;
	}

	public int getCloseAccl() {
		return closeAccl;
	}

	public void setCloseAccl(int closeAccl) {
		this.closeAccl = closeAccl;
	}

	public int getCloseCheckPct() {
		return closeCheckPct;
	}

	public void setCloseCheckPct(int closeCheckPct) {
		this.closeCheckPct = closeCheckPct;
	}

	public int getCloseCheckSpeed() {
		return closeCheckSpeed;
	}

	public void setCloseCheckSpeed(int closeCheckSpeed) {
		this.closeCheckSpeed = closeCheckSpeed;
	}

	public int getCloseCheckTorque() {
		return closeCheckTorque;
	}

	public void setCloseCheckTorque(int closeCheckTorque) {
		this.closeCheckTorque = closeCheckTorque;
	}

	public int getCloseBraking() {
		return closeBraking;
	}

	public void setCloseBraking(int closeBraking) {
		this.closeBraking = closeBraking;
	}

	public int getClosePress() {
		return closePress;
	}

	public void setClosePress(int closePress) {
		this.closePress = closePress;
	}

	public int getFuncSwitchType() {
		return funcSwitchType;
	}

	public void setFuncSwitchType(int funcSwitchType) {
		this.funcSwitchType = funcSwitchType;
	}

	public int getLockTime() {
		return lockTime;
	}

	public void setLockTime(int lockTime) {
		this.lockTime = lockTime;
	}

	public int getDelayedEgress() {
		return delayedEgress;
	}

	public void setDelayedEgress(int delayedEgress) {
		this.delayedEgress = delayedEgress;
	}

	public int getAccessCtrlPro() {
		return accessCtrlPro;
	}

	public void setAccessCtrlPro(int accessCtrlPro) {
		this.accessCtrlPro = accessCtrlPro;
	}

	public int getIoConfig() {
		return ioConfig;
	}

	public void setIoConfig(int ioConfig) {
		this.ioConfig = ioConfig;
	}

	public int getFireAlarmMode() {
		return fireAlarmMode;
	}

	public void setFireAlarmMode(int fireAlarmMode) {
		this.fireAlarmMode = fireAlarmMode;
	}

	public int getM1FullOpenOffset() {
		return m1FullOpenOffset;
	}

	public void setM1FullOpenOffset(int m1FullOpenOffset) {
		this.m1FullOpenOffset = m1FullOpenOffset;
	}

	public int getM2FullOpenOffset() {
		return m2FullOpenOffset;
	}

	public void setM2FullOpenOffset(int m2FullOpenOffset) {
		this.m2FullOpenOffset = m2FullOpenOffset;
	}

	public int getM1FullOpenTorque() {
		return m1FullOpenTorque;
	}

	public void setM1FullOpenTorque(int m1FullOpenTorque) {
		this.m1FullOpenTorque = m1FullOpenTorque;
	}

	public int getM2FullOpenTorque() {
		return m2FullOpenTorque;
	}

	public void setM2FullOpenTorque(int m2FullOpenTorque) {
		this.m2FullOpenTorque = m2FullOpenTorque;
	}

	public int getM1MtSensitivity() {
		return m1MtSensitivity;
	}

	public void setM1MtSensitivity(int m1MtSensitivity) {
		this.m1MtSensitivity = m1MtSensitivity;
	}

	public int getM2MtSensitivity() {
		return m2MtSensitivity;
	}

	public void setM2MtSensitivity(int m2MtSensitivity) {
		this.m2MtSensitivity = m2MtSensitivity;
	}

	public int getM1MtHoldOpenTime() {
		return m1MtHoldOpenTime;
	}

	public void setM1MtHoldOpenTime(int m1MtHoldOpenTime) {
		this.m1MtHoldOpenTime = m1MtHoldOpenTime;
	}

	public int getM2MtHoldOpenTime() {
		return m2MtHoldOpenTime;
	}

	public void setM2MtHoldOpenTime(int m2MtHoldOpenTime) {
		this.m2MtHoldOpenTime = m2MtHoldOpenTime;
	}

	public int getM1CloseCheckLen() {
		return m1CloseCheckLen;
	}

	public void setM1CloseCheckLen(int m1CloseCheckLen) {
		this.m1CloseCheckLen = m1CloseCheckLen;
	}

	public int getM2CloseCheckLen() {
		return m2CloseCheckLen;
	}

	public int getM1CloseCheckBoost() {
		return m1CloseCheckBoost;
	}

	public void setM1CloseCheckBoost(int m1CloseCheckBoost) {
		this.m1CloseCheckBoost = m1CloseCheckBoost;
	}

	public int getM2CloseCheckBoost() {
		return m2CloseCheckBoost;
	}

	public void setM2CloseCheckBoost(int m2CloseCheckBoost) {
		this.m2CloseCheckBoost = m2CloseCheckBoost;
	}

	public void setM2CloseCheckLen(int m2CloseCheckLen) {
		this.m2CloseCheckLen = m2CloseCheckLen;
	}

	public int getM1PowerCloseSpeed() {
		return m1PowerCloseSpeed;
	}

	public void setM1PowerCloseSpeed(int m1PowerCloseSpeed) {
		this.m1PowerCloseSpeed = m1PowerCloseSpeed;
	}

	public int getM2PowerCloseSpeed() {
		return m2PowerCloseSpeed;
	}

	public void setM2PowerCloseSpeed(int m2PowerCloseSpeed) {
		this.m2PowerCloseSpeed = m2PowerCloseSpeed;
	}

	public int getM1PowerCloseTorque() {
		return m1PowerCloseTorque;
	}

	public void setM1PowerCloseTorque(int m1PowerCloseTorque) {
		this.m1PowerCloseTorque = m1PowerCloseTorque;
	}

	public int getM2PowerCloseTorque() {
		return m2PowerCloseTorque;
	}

	public void setM2PowerCloseTorque(int m2PowerCloseTorque) {
		this.m2PowerCloseTorque = m2PowerCloseTorque;
	}

	public int getM1PowerCloseObsTime() {
		return m1PowerCloseObsTime;
	}

	public void setM1PowerCloseObsTime(int m1PowerCloseObsTime) {
		this.m1PowerCloseObsTime = m1PowerCloseObsTime;
	}

	public int getM2PowerCloseObsTime() {
		return m2PowerCloseObsTime;
	}

	public void setM2PowerCloseObsTime(int m2PowerCloseObsTime) {
		this.m2PowerCloseObsTime = m2PowerCloseObsTime;
	}

	public int getM1PowerCloseLen() {
		return m1PowerCloseLen;
	}

	public void setM1PowerCloseLen(int m1PowerCloseLen) {
		this.m1PowerCloseLen = m1PowerCloseLen;
	}

	public int getM2PowerCloseLen() {
		return m2PowerCloseLen;
	}

	public void setM2PowerCloseLen(int m2PowerCloseLen) {
		this.m2PowerCloseLen = m2PowerCloseLen;
	}

	public int getM1PowerCloseTm() {
		return m1PowerCloseTm;
	}

	public void setM1PowerCloseTm(int m1PowerCloseTm) {
		this.m1PowerCloseTm = m1PowerCloseTm;
	}

	public int getM2PowerCloseTm() {
		return m2PowerCloseTm;
	}

	public void setM2PowerCloseTm(int m2PowerCloseTm) {
		this.m2PowerCloseTm = m2PowerCloseTm;
	}

	public int getM1StallSpeed() {
		return m1StallSpeed;
	}

	public void setM1StallSpeed(int m1StallSpeed) {
		this.m1StallSpeed = m1StallSpeed;
	}

	public int getM2StallSpeed() {
		return m2StallSpeed;
	}

	public void setM2StallSpeed(int m2StallSpeed) {
		this.m2StallSpeed = m2StallSpeed;
	}

	public int getM1StallTorque() {
		return m1StallTorque;
	}

	public void setM1StallTorque(int m1StallTorque) {
		this.m1StallTorque = m1StallTorque;
	}

	public int getM2StallTorque() {
		return m2StallTorque;
	}

	public void setM2StallTorque(int m2StallTorque) {
		this.m2StallTorque = m2StallTorque;
	}

	public int getM1OpDelay() {
		return m1OpDelay;
	}

	public void setM1OpDelay(int m1OpDelay) {
		this.m1OpDelay = m1OpDelay;
	}

	public int getM2OpDelay() {
		return m2OpDelay;
	}

	public void setM2OpDelay(int m2OpDelay) {
		this.m2OpDelay = m2OpDelay;
	}

	public int getM1DoorTravel() {
		return m1DoorTravel;
	}

	public void setM1DoorTravel(int m1DoorTravel) {
		this.m1DoorTravel = m1DoorTravel;
	}

	public int getM2DoorTravel() {
		return m2DoorTravel;
	}

	public void setM2DoorTravel(int m2DoorTravel) {
		this.m2DoorTravel = m2DoorTravel;
	}

	public int getOverspeedCompRate() {
		return overspeedCompRate;
	}

	public void setOverspeedCompRate(int overspeedCompRate) {
		this.overspeedCompRate = overspeedCompRate;
	}

	public int getOffMode() {
		return offMode;
	}

	public void setOffMode(int offMode) {
		this.offMode = offMode;
	}

	public int getUnlockTime() {
		return unlockTime;
	}

	public void setUnlockTime(int unlockTime) {
		this.unlockTime = unlockTime;
	}

	public int getSensorOverride() {
		return sensorOverride;
	}

	public void setSensorOverride(int sensorOverride) {
		this.sensorOverride = sensorOverride;
	}

	public int getNoRevObs() {
		return noRevObs;
	}

	public void setNoRevObs(int noRevObs) {
		this.noRevObs = noRevObs;
	}

	public int getCloseWoOb() {
		return closeWoOb;
	}

	public void setCloseWoOb(int closeWoOb) {
		this.closeWoOb = closeWoOb;
	}

	public int getOptionThree() {
		return optionThree;
	}

	public void setOptionThree(int optionThree) {
		this.optionThree = optionThree;
	}

	public int getOptionFour() {
		return optionFour;
	}

	public void setOptionFour(int optionFour) {
		this.optionFour = optionFour;
	}

	public int getM1OpenCheckBoost() {
		return m1OpenCheckBoost;
	}

	public void setM1OpenCheckBoost(int m1OpenCheckBoost) {
		this.m1OpenCheckBoost = m1OpenCheckBoost;
	}

	public int getM2OpenCheckBoost() {
		return m2OpenCheckBoost;
	}

	public void setM2OpenCheckBoost(int m2OpenCheckBoost) {
		this.m2OpenCheckBoost = m2OpenCheckBoost;
	}

	public int getOpenPowerAssistTorque() {
		return openPowerAssistTorque;
	}

	public void setOpenPowerAssistTorque(int openPowerAssistTorque) {
		this.openPowerAssistTorque = openPowerAssistTorque;
	}

	public int getClosePowerAssistTorque() {
		return closePowerAssistTorque;
	}

	public void setClosePowerAssistTorque(int closePowerAssistTorque) {
		this.closePowerAssistTorque = closePowerAssistTorque;
	}

	public int getM1OpenLearnObstructionTime() {
		return m1OpenLearnObstructionTime;
	}

	public void setM1OpenLearnObstructionTime(int m1OpenLearnObstructionTime) {
		this.m1OpenLearnObstructionTime = m1OpenLearnObstructionTime;
	}

	public int getM2OpenLearnObstructionTime() {
		return m2OpenLearnObstructionTime;
	}

	public void setM2OpenLearnObstructionTime(int m2OpenLearnObstructionTime) {
		this.m2OpenLearnObstructionTime = m2OpenLearnObstructionTime;
	}

	public int getM1OpenStartupObstructionTime() {
		return m1OpenStartupObstructionTime;
	}

	public void setM1OpenStartupObstructionTime(int m1OpenStartupObstructionTime) {
		this.m1OpenStartupObstructionTime = m1OpenStartupObstructionTime;
	}

	public int getM2OpenStartupObstructionTime() {
		return m2OpenStartupObstructionTime;
	}

	public void setM2OpenStartupObstructionTime(int m2OpenStartupObstructionTime) {
		this.m2OpenStartupObstructionTime = m2OpenStartupObstructionTime;
	}

	public int getM1PowerAssistTorque() {
		return m1PowerAssistTorque;
	}

	public void setM1PowerAssistTorque(int m1PowerAssistTorque) {
		this.m1PowerAssistTorque = m1PowerAssistTorque;
	}

	public int getM2PowerAssistTorque() {
		return m2PowerAssistTorque;
	}

	public void setM2PowerAssistTorque(int m2PowerAssistTorque) {
		this.m2PowerAssistTorque = m2PowerAssistTorque;
	}

	public int getM1PowerAssistExitAngle() {
		return m1PowerAssistExitAngle;
	}

	public void setM1PowerAssistExitAngle(int m1PowerAssistExitAngle) {
		this.m1PowerAssistExitAngle = m1PowerAssistExitAngle;
	}

	public int getM2PowerAssistExitAngle() {
		return m2PowerAssistExitAngle;
	}

	public void setM2PowerAssistExitAngle(int m2PowerAssistExitAngle) {
		this.m2PowerAssistExitAngle = m2PowerAssistExitAngle;
	}

	public int getM1PowerAssistTriggerThreshold() {
		return m1PowerAssistTriggerThreshold;
	}

	public void setM1PowerAssistTriggerThreshold(int m1PowerAssistTriggerThreshold) {
		this.m1PowerAssistTriggerThreshold = m1PowerAssistTriggerThreshold;
	}

	public int getM2PowerAssistTriggerThreshold() {
		return m2PowerAssistTriggerThreshold;
	}

	public void setM2PowerAssistTriggerThreshold(int m2PowerAssistTriggerThreshold) {
		this.m2PowerAssistTriggerThreshold = m2PowerAssistTriggerThreshold;
	}

	public int getM1CloseObsTime() {
		return m1CloseObsTime;
	}

	public void setM1CloseObsTime(int m1CloseObsTime) {
		this.m1CloseObsTime = m1CloseObsTime;
	}

	public int getM2CloseObsTime() {
		return m2CloseObsTime;
	}

	public void setM2CloseObsTime(int m2CloseObsTime) {
		this.m2CloseObsTime = m2CloseObsTime;
	}

	public int getM1StallSensorInput() {
		return m1StallSensorInput;
	}

	public void setM1StallSensorInput(int m1StallSensorInput) {
		this.m1StallSensorInput = m1StallSensorInput;
	}

	public int getM2StallSensorInput() {
		return m2StallSensorInput;
	}

	public void setM2StallSensorInput(int m2StallSensorInput) {
		this.m2StallSensorInput = m2StallSensorInput;
	}

	public int getM1MagicTouchFeel() {
		return m1MagicTouchFeel;
	}

	public void setM1MagicTouchFeel(int m1MagicTouchFeel) {
		this.m1MagicTouchFeel = m1MagicTouchFeel;
	}

	public int getM2MagicTouchFeel() {
		return m2MagicTouchFeel;
	}

	public void setM2MagicTouchFeel(int m2MagicTouchFeel) {
		this.m2MagicTouchFeel = m2MagicTouchFeel;
	}

	public int getM1MotorStartSpeed() {
		return m1MotorStartSpeed;
	}

	public void setM1MotorStartSpeed(int m1MotorStartSpeed) {
		this.m1MotorStartSpeed = m1MotorStartSpeed;
	}

	public int getM2MotorStartSpeed() {
		return m2MotorStartSpeed;
	}

	public void setM2MotorStartSpeed(int m2MotorStartSpeed) {
		this.m2MotorStartSpeed = m2MotorStartSpeed;
	}

	public int getM1CreepLength() {
		return m1CreepLength;
	}

	public void setM1CreepLength(int m1CreepLength) {
		this.m1CreepLength = m1CreepLength;
	}

	public int getM2CreepLength() {
		return m2CreepLength;
	}

	public void setM2CreepLength(int m2CreepLength) {
		this.m2CreepLength = m2CreepLength;
	}

	public int getM1PreloadTorque() {
		return m1PreloadTorque;
	}

	public void setM1PreloadTorque(int m1PreloadTorque) {
		this.m1PreloadTorque = m1PreloadTorque;
	}

	public int getM2PreloadTorque() {
		return m2PreloadTorque;
	}

	public void setM2PreloadTorque(int m2PreloadTorque) {
		this.m2PreloadTorque = m2PreloadTorque;
	}

	public int getM1PreloadMode() {
		return m1PreloadMode;
	}

	public void setM1PreloadMode(int m1PreloadMode) {
		this.m1PreloadMode = m1PreloadMode;
	}

	public int getM2PreloadMode() {
		return m2PreloadMode;
	}

	public void setM2PreloadMode(int m2PreloadMode) {
		this.m2PreloadMode = m2PreloadMode;
	}

	public int getM1PowerAssistSensitivity() {
		return m1PowerAssistSensitivity;
	}

	public void setM1PowerAssistSensitivity(int m1PowerAssistSensitivity) {
		this.m1PowerAssistSensitivity = m1PowerAssistSensitivity;
	}

	public int getM2PowerAssistSensitivity() {
		return m2PowerAssistSensitivity;
	}

	public void setM2PowerAssistSensitivity(int m2PowerAssistSensitivity) {
		this.m2PowerAssistSensitivity = m2PowerAssistSensitivity;
	}

	public Date getInsertTS() {
		return insertTS;
	}

	public void setInsertTS(Date insertTS) {
		this.insertTS = insertTS;
	}

	public Date getUpdateTS() {
		return updateTS;
	}

	public void setUpdateTS(Date updateTS) {
		this.updateTS = updateTS;
	}

	public int getM1RecycleSpeed() {
		return m1RecycleSpeed;
	}

	public void setM1RecycleSpeed(int m1RecycleSpeed) {
		this.m1RecycleSpeed = m1RecycleSpeed;
	}

	public int getM2RecycleSpeed() {
		return m2RecycleSpeed;
	}

	public void setM2RecycleSpeed(int m2RecycleSpeed) {
		this.m2RecycleSpeed = m2RecycleSpeed;
	}

	public int getM1OpenStop() {
		return m1OpenStop;
	}

	public void setM1OpenStop(int m1OpenStop) {
		this.m1OpenStop = m1OpenStop;
	}

	public int getM2OpenStop() {
		return m2OpenStop;
	}

	public void setM2OpenStop(int m2OpenStop) {
		this.m2OpenStop = m2OpenStop;
	}

	public int getM1CreepSpeed() {
		return m1CreepSpeed;
	}

	public void setM1CreepSpeed(int m1CreepSpeed) {
		this.m1CreepSpeed = m1CreepSpeed;
	}

	public int getM2CreepSpeed() {
		return m2CreepSpeed;
	}

	public void setM2CreepSpeed(int m2CreepSpeed) {
		this.m2CreepSpeed = m2CreepSpeed;
	}

	
	
}
