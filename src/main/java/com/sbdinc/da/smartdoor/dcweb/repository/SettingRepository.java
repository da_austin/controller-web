package com.sbdinc.da.smartdoor.dcweb.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.sbdinc.da.smartdoor.dcweb.model.Setting;

/**
 * @author Dian Jiao
 *
 */
public interface SettingRepository extends JpaRepository<Setting, Long> {

}
