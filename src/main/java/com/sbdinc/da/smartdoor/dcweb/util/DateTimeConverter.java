package com.sbdinc.da.smartdoor.dcweb.util;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Util Class for Datetime conversion
 *
 * @author Dian Jiao
 *
 */
public class DateTimeConverter {
	private static final Logger logger = LoggerFactory.getLogger(DateTimeConverter.class);
	private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
	
	public static LocalDateTime stringToDateTime(String dtStr) {
		LocalDateTime dt = null; 
		// Epoch time conversion
		if (dtStr.matches("[0-9]+")) {
			if (dtStr.length() == 10) 
				dt = LocalDateTime.ofInstant(Instant.ofEpochSecond(Long.parseLong(dtStr)), ZoneId.systemDefault());
			else if (dtStr.length() == 13)
				dt = LocalDateTime.ofInstant(Instant.ofEpochMilli(Long.parseLong(dtStr)), ZoneId.systemDefault());
		}
		else {
			try {
				dt = LocalDateTime.parse(dtStr, FORMATTER);
			}
			catch (DateTimeParseException e) {
				logger.warn(e.getMessage());
			}
		}
		return dt;
	}
}
