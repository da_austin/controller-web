package com.sbdinc.da.smartdoor.dcweb.service;

/**
 * Interface of Message Service
 * 
 * @author Dian Jiao
 * @param <T>
 *
 */
public interface MessageService {
	long saveMessage(String source, String type, String format, String trigger, String msgBody);
	void updateMessage(long msgId, char code, String note);
}
