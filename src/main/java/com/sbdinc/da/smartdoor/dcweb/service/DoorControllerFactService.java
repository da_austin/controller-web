package com.sbdinc.da.smartdoor.dcweb.service;

import com.sbdinc.da.smartdoor.dcweb.model.DoorControllerFact;

/**
 * @author Dian Jiao
 *
 */
public interface DoorControllerFactService {
	long saveControllerDetail(DoorControllerFact controllerDetail);
	long findIDbyDoorId(String doorId);
	
}
