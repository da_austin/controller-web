package com.sbdinc.da.smartdoor.dcweb.service;

import java.io.IOException;
import java.text.ParseException;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.xml.sax.SAXException;

import com.sbdinc.da.smartdoor.dcweb.repository.ErrorHistoryRepository;

/**
 * @author Dian Jiao
 *
 */
public class ErrorHistoryServiceImplTest {
	
	@Mock
	private ErrorHistoryRepository repository;
	
	@Mock
	private DoorControllerFactService doorControllerFactService;
	
	@InjectMocks
	private ErrorHistoryServiceImpl service;
	

	@Before
	public void setup(){
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void test() {
		StringBuilder sb = new StringBuilder();
		sb.append("<?xml version='1.0' encoding='UTF-8' standalone='yes' ?>\r\n");
		sb.append("<errorHistory>\r\n"); 
		sb.append("  <doorId>d001</doorId>\r\n"); 
		sb.append("  <datetime>2017-08-30T14:30:48</datetime>\r\n"); 
		sb.append("  <timezone>-10</timezone>\r\n"); 
		sb.append("  <cycles>156511</cycles>\r\n"); 
		sb.append("  <period>727</period>\r\n"); 
		sb.append("  <errors>\r\n"); 
		sb.append("    <error>\r\n"); 
		sb.append("      <errorCode>1</errorCode>\r\n"); 
		sb.append("      <name>Obstruction On Close</name>\r\n"); 
		sb.append("	  <description> Cannot close the door</description>\r\n"); 
		sb.append("      <doors>\r\n"); 
		sb.append("        <door>\r\n"); 
		sb.append("          <doorNumber>1</doorNumber>\r\n"); 
		sb.append("          <count>7</count>\r\n"); 
		sb.append("          <lastCycleOccurence>156506</lastCycleOccurence>\r\n"); 
		sb.append("        </door>\r\n"); 
		sb.append("		<door>\r\n"); 
		sb.append("          <doorNumber>2</doorNumber>\r\n"); 
		sb.append("          <count>2</count>\r\n"); 
		sb.append("          <lastCycleOccurence>156393</lastCycleOccurence>\r\n"); 
		sb.append("        </door>\r\n"); 
		sb.append("      </doors>\r\n"); 
		sb.append("    </error>\r\n"); 
		sb.append("    <error>\r\n"); 
		sb.append("      <errorCode>3</errorCode>\r\n"); 
		sb.append("      <name>Breakout</name>\r\n"); 
		sb.append("      <doors>\r\n"); 
		sb.append("        <door>\r\n"); 
		sb.append("          <doorNumber>1</doorNumber>\r\n"); 
		sb.append("          <count>2</count>\r\n"); 
		sb.append("          <lastCycleOccurence>156456</lastCycleOccurence>\r\n"); 
		sb.append("        </door>\r\n"); 
		sb.append("      </doors>\r\n"); 
		sb.append("      <value>2</value>\r\n"); 
		sb.append("    </error>\r\n"); 
		sb.append("  </errors>\r\n"); 
		sb.append("</errorHistory>");
		String xml = sb.toString();
		try {
			service.saveErrorHistory(xml, 1, 1);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
