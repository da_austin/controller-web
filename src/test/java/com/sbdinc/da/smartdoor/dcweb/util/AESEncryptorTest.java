package com.sbdinc.da.smartdoor.dcweb.util;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

/**
 *  Unit test for AESEncryptor
 *
 * @author Dian Jiao
 *
 */
public class AESEncryptorTest {

	@Test
	public void testDecryption() throws Exception {
		
		String encrypted = "K8+09j2NUI06XsFG8d0JF+aPmCsZ1XczQP9cWSXiadU=";
		String email = "stanley@sbdinc.com";
		String decrypt = AESEncryptor.decrypt(encrypted);
		assertEquals(email, decrypt);
	}
	
	@Ignore
	@Test
	public void testEncryption() throws Exception {
		String email = "stanley@sbdinc.com";
		System.out.println(AESEncryptor.encrypt(email));
	}

}
